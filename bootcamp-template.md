### Overview
**Goal**: <general goal of bootcamp>

*Length*: <estimate # of hours, use single number or range>

**Objectives**: At the end of this bootcamp, you should be able to:
- <insert a few concrete actions that learner should be able to do at the end of this bootcamp>

<any special instructions, like the order of stages>

### Stage 0: Create Your Bootcamp
1. [ ] Create an issue using this template by making the Issue Title: <bootcamp title> - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Set milestones, if applicable, and a due date to help motivate yourself!

Consider using the Time Tracking functionality so that the estimated length for the bootcamp can be refined.

### Stage 1: <section name>

1. [ ] Task with any relevant link

### Stage X: Tickets

- [ ] **Done with Stage X**

1. [ ] Answer 5 <feature> tickets and paste the links here. Do this even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers.

   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

### Stage X: Pair on Customer Calls

- [ ] **Done with Stage X**

1. [ ] Pair on two diagnostic calls, where a customer is having trouble with GitLab <feature>.
   1. [ ] call with ___
   1. [ ] call with ___

### Penultimate Stage: Review
You feel that you can now do all of the objectives:

- copy the list of relevant objectives

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this bootcamp or in other documentation, list it below as tasks for yourself!

- [ ] Update ...

### Final Stage: Completion

1. [ ] Have your trainer and manager review this issue.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the bootcamp went once you have reviewed this issue.
1. [ ] Submit a MR to add "<topic> Expert" on the team page.
1. [ ] Once complete, add this bootcamp to the list of training you have completed!

/label ~bootcamp
