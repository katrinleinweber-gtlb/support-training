Title: "ElasticSearch Bootcamp - your-name"

## Goal of this checklist

> Set a clear path for ElasticSearch Expert training

## Objectives:

* Learn about ElasticSearch
* Learn about GitLab's ElasticSearch integration
* Feel comfortable answering some common scenarios.

---

* ### Stage 1: Commit and become familiar with what ElasticSearch is
  * [ ] Read ElasticSearch Documentation
    * [ ] Read [What is ElasticSearch?](https://www.elastic.co/what-is/elasticsearch)
    * [ ] Read [Elasticsearch Reference – Basic Concepts](https://www.elastic.co/guide/en/elasticsearch/reference/current/getting-started-concepts.html)
    * [ ] Read [Elasticsearch Reference – Reading and Writing Documents](https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-replication.html)
    * [ ] Read [Elasticsearch Reference – Analysis](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis.html)
  * [ ] Read the GitLab Documentation
    * [ ] Read [Elasticsearch integration](https://docs.gitlab.com/ee/integration/elasticsearch.html)
    * [ ] Read [Troubleshooting Elasticsearch](https://docs.gitlab.com/ee/administration/troubleshooting/elasticsearch.html)
    * [ ] Read [The challenge of enabling Elasticsearch on GitLab.com](https://about.gitlab.com/blog/2019/07/16/elasticsearch-update/)
* ### Stage 2: Technical setup
  * [ ] Spin up an ElasticSearch installation. This can be via VM or docker.
  * [ ] Follow [Elasticsearch integration](https://docs.gitlab.com/ee/integration/elasticsearch.html)
        to integrate said ElasticSearch installation with a running GitLab
        installation.
  * [ ] Practice indexing and re-indexing the whole GitLab instance.
  * [ ] Practice re-indexing a specific project.
  * [ ] Feel comfortable determining which projects are not indexed.
* ### Stage 3: Quiz
  * [ ] Contact a current ElasticSearch master and let them know you are ready
        for the quiz. They will provide you 3 scenarios in the comment section
        below. You will reply to each scenario as if it was a ticket. You should
        treat these as customer replies and attempt to fully resolve the
        scenario in **one** reply. Example scenarios can be found
        [here](../../content/bootcamp-elasticsearch/scenarios.md).
  * [ ] Schedule a call with a current ElasticSearch master. During this call,
        you will guide them through the following:
    * [ ] Integrating a running ElasticSearch installation with GitLab. This
          does include fully indexing all your data. **Pro-tip**: Do it with
          a smaller data set to speed up the process.
    * [ ] Via the ElasticSearch API, pull the following:
      * [ ] The current health status of the ElasticSearch installation.
      * [ ] Information about the index you just created.
    * [ ] Clear the index status for one project and then re-index that project.
    * [ ] Perform a search for a basic term and verify the results are the same
          via the following methods:
      * [ ] The rails console
      * [ ] The ElasticSearch Search API
      * [ ] The GitLab Search UI
  * [ ] Once you have completed this, have the master comment below
        acknowledging your success.
* ### Final Stage:
  * [ ] Your Manager needs to check this box to acknowledge that you finished
  * [ ] Send a MR to declare yourself an Omnibus Expert on the team page

/label bootcamp
/epic &16