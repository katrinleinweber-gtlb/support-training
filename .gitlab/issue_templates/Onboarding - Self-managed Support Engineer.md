Welcome to your Self-managed Support onboarding issue!

> **As you work through this onboarding issue, please remember that this issue is [non-confidential and is public](https://about.gitlab.com/handbook/values/#transparency). With that said, please do not add any confidential data such as customer names, logs, etc. to this issue. Linking to the appropriate [Zendesk ticket](https://about.gitlab.com/handbook/support/workflows/working_with_security.html#general-guidelines-1) is OK!**


We believe that people often learn best and fastest by diving into (relatively easy) tickets and learning by doing. However, this has to be balanced with making time to learn some of the tougher topics. You should start tackling **stages 0, 1, 2, and 3** as early as your first week, though you are not likely to complete all items for at least several weeks. The expectation is therefore that you are sufficiently advanced in your onboarding by the end of your first week that you can answer 2-5 "simple" customer tickets.

Over the following weeks, your manager will set higher targets for the number and difficulty of tickets to be tackled, you should always save about 2-3 hours per day to spend on working through this boot camp. Some days it will be less, some days it will be more, depending on your working style and ticket load; however an average of 2-3 hours per day should allow you to complete everything through to the end of stage 6 within about four weeks.

> _We need to keep iterating on this checklist so please submit MR's for any improvements
that you can think of. The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates)
If an item does not start with a role or someone else's name, it's yours to do._

> **The goal of this entire checklist:** To set a clear path for training a Gitlab Support Engineer

## Manager Tasks pre-Stage 0

- [ ] Open Support Engineering onboarding issue
- [ ] Open [Support Engineering access request issue](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Support_Engineer)
- [ ] Add new team member to:
    - regional call
- [ ] Schedule 1:1 meeting
    - [ ] Create 1:1 doc
    - [ ] Schedule 1st day
- [ ] Add new team member to calendars:
    - [ ] GitLab Support
    - [ ] Support Time Off
- [ ] Verify new team member has updated BambooHR with their primary and other citizenships
   - [ ] Verify if new team member is a US citizen living in the US
   - [ ] (If they are) add them to the Federal Zendesk instance as Staff
- [ ] Check Stage 0 for access request checklist, and other manager tasks.

## Your trainer: `[tag trainer]`

### Stage 0: Complete general onboarding to have all necessary accounts and permissions

- [ ] **Done with Stage 0**

_Typically completed within the first week_

1. [ ] General Onboarding Checklist: this should have been created for you on [the People Ops Employment issue tracker](https://gitlab.com/gitlab-com/people-ops/employment/issues/)
   when you were hired.
   1. [ ] Finish every item on that list starting with `New team member:` until you
      are waiting for someone to answer a question or do something before you can continue that list.
  1. [ ] Ensure that as part of general onboarding, you and your manager check in that you have been added to all relevant Engineering communications channels. Access can be requested via a [Single Person Access Request](https://about.gitlab.com/handbook/business-ops/it-ops-team/access-requests/#single-person-access-request). Relevant channels include:
      - [ ] Email list (Google Groups)
        - engineering@gitlab.com
        - supportteam@gitlab.com
      - Google Drive shared drives (auto access via Google Groups)
        - Support
        - Engineering
      - [ ] 1password Support Vault
      - [ ] In ZenDesk as Staff in Support Group
      - [ ] Slack
        - Join `#support-team-chat`, `#support_gitlab-com` (optional), `#support_self-managed`, `#support-managers`
        - Added to `@support-dotcom` and/or `@support-selfmanaged` group
        - Added to `@support-team-amer`, `@support-team-emea` or `@support-team-apac` group
      - [ ] GitLab groups and aliases:
        - [ ] _Manager:_ add team member to `gitlab-com/support` as `Maintainer`
        - [ ] _Manager:_ add team member to regional group `gitlab-com/support/amer`, `gitlab-com/support/apac` or `gitlab-com/support/emea` as `Owner`
   1. [ ] Start Stage 1 here, as well as the first steps of Stage 2 and Stage 3.
   1. [ ] Check back daily on what was blocking you on your General Onboarding Checklist
      until that list is completely done, then focus on this one.
1. [ ] Team Calls: To keep up with the changes and happenings within the Support team, we have team calls every week. Every member of the support team is encouraged to join so they can also state their opinions during the call.
   1. [ ] Read up on the [calls the Support Team uses to sync up](https://about.gitlab.com/handbook/support/#weekly-meetings) and make sure you have the ones that pertain to you on your calendar.
   1. [ ] Identify agendas for those meetings, and read through a few past meetings in the document.
   1. [ ] Verify that you have a 1:1 scheduled with your manager and you have access to the agenda for that meeting.
1. [ ] Introduce yourself to your team!
   1. [ ] Write an entry with your name, location, the date you started, a quick blurb about your experience, personal interests and what drew you to GitLab support in the [Support Week in Review doc](https://docs.google.com/document/d/1eyMzbzImSKNFMpmu33C6imvC1iWEWHREJqaD6mkVDNg/edit) before Friday.
      1. [ ] Manager: Make sure to introduce the team member in the [Engineering Week-in-Review document](https://docs.google.com/document/d/1Oglq0-rLbPFRNbqCDfHT0-Y3NkVEiHj6UukfYijHyUs/edit#) and copy the introduction over to [Support Week in Review](https://docs.google.com/document/d/1eyMzbzImSKNFMpmu33C6imvC1iWEWHREJqaD6mkVDNg/edit#heading=h.pykb7ntmpjic)!
      1. [ ] Manager: If the new team member is in a region that does a regional team meeting, talk to the new team member about doing an introduction at the next regional team meeting, and add it to the agenda.
   1. [ ] Now, also format and send the introduction post you just created to the `#support-team-chat` Slack channel. Welcome to [multi-modal communication](https://about.gitlab.com/handbook/communication/#multimodal-communication), key to effective communication at GitLab!


### Stage 1: Become familiar with git and GitLab basics

- [ ] **Done with Stage 1**

_Typically started in the first week, completed by the end of the second week_

If you are already comfortable with using Git and GitLab and you are able to
retain a good amount of information by just watching or reading through, that is
okay. But if you see a topic that is completely new to you, stop the video and try
it out for yourself before continuing.

1. [ ] Let your manager know you're ready to be assigned a trainer.
1. [ ] Add your **Primary Citizenship** and **Additional Citizenship(s)** to the fields in BambooHR on the Personal tab if you haven't already.
   - [ ] Manager: if they are US Citizens based in the US, add them to the Federal ZD instance.
1. [ ] Just quickly check on your Zendesk account to make sure that is ready for you when you need it.
1. [ ] Add a [profile picture](https://support.zendesk.com/hc/en-us/articles/203690996-Updating-your-user-profile-and-password#topic_rgk_2z2_kh)
   to your Zendesk account
1. [ ] Let your manager know if you were not able to create an account in Zendesk for some reason.
1. [ ] Under your profile in Zendesk, it should read `Support Staff`. If it reads `Light Agent`, inform your manager.
1. [ ] Get familiar with our [Support Workflows](https://about.gitlab.com/handbook/support/workflows/#support_workflows) & [Statement of Support](https://about.gitlab.com/support/statement-of-support.html)
1. [ ] [Request access to Chatops](https://docs.gitlab.com/ee/development/chatops_on_gitlabcom.html#chatops-on-gitlabcom)
1. [ ] Watch this [video about gitlab debug techniques and architecture overview](https://www.youtube.com/watch?v=9W6QnpYewik)

Go over these topics in [GitLab University](https://docs.gitlab.com/ee/university/):

1. [ ] Under the topic of [Version Control and Git](https://docs.gitlab.com/ee/university/#1-1-version-control-and-git)
   1. [ ] [About Version Control](https://docs.google.com/presentation/d/16sX7hUrCZyOFbpvnrAFrg6tVO5_yT98IgdAqOmXwBho/edit#slide=id.gd69537a19_0_14)
   1. [ ] [Try Git](https://www.katacoda.com/courses/git)
   1. [ ] Explore [Git internals](https://git-scm.com/book/en/v2/Git-Internals-Plumbing-and-Porcelain)
     and go back to it from time to time to learn more about how Git works
1. [ ] Under the topic of [GitLab Basics](https://docs.gitlab.com/ee/university/#1-2-gitlab-basics)
   1. [ ] All the [GitLab Basics](http://docs.gitlab.com/ee/gitlab-basics/README.html)
      that you don't feel comfortable with. If you get stuck, see the linked videos
      under GitLab Basics in GitLab University
   1. [ ] [GitLab Flow](https://www.youtube.com/watch?v=UGotqAUACZA)
   1. [ ] Take a look at how the different GitLab versions [compare](https://about.gitlab.com/features/)
1. [ ] Any of these topics that you don't feel comfortable with in the [user training](https://gitlab.com/gitlab-org/gitlab-ee/tree/master/doc/university/training/topics)
   we use for our customers: `env_setup.md`, `feature_branching.md`, `stash.md`, `git_log.md`.
   For the rest of the topics in `user training`, just do a quick read over the file names
   so you start remembering where to find them.
1. [ ] Get familiar with the services GitLab offers
   1. [ ] The differences between [CE and EE (Core, Starter, Premium, Ultimate)](https://about.gitlab.com/pricing/#self-managed)
1. [ ] Get familiar with the [different teams in-charge of every stage in the DevOps cycle](https://about.gitlab.com/handbook/product/categories/#devops-stages) and what they are responsible for. This will assist you in adding the right labels when creating issues and escalating in the right Slack channel.

### Stage 2. Installation and Administration basics.

- [ ] **Done with Stage 2**

_Typically started in the first week, completed by the end of the third week_

**Goals**

- Get your development machine set up.
- Familiarize yourself with the codebase.
- Be prepared to reproduce issues that our users encounter.
- Be comfortable with the different installation options of GitLab and configure LDAP.
- Have an installation available for reproducing customer bug reports.

1. [ ] Install [this masterpiece](https://gitlab.com/gitlab-com/support/toolbox/zd-dl-router) created by Anton Smith, you'll thank yourself later for doing this.
1. [ ] Check back on your Zendesk account to see if you are an `Support Staff` yet.
1. [ ] Manager: Add team member to the `GitLab Support` Google Calendar.
- [ ] Add team member to the `GitLab Support` Google Cloud Project with `Support Permissions`.


**Set up your development machine**

1. [ ] Install the [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit). If you like Docker (especially if you're running Linux
    on your desktop - you might like to consider [GitLab Compose Kit](https://gitlab.com/gitlab-org/gitlab-compose-kit) instead.)
2. [ ] Add [OpenLDAP](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/ldap.md)
    to your GDK installation.

**Installations**

You will keep one installation continually updated on Digital Ocean (managed through terraform),
just like many of our clients, but you need to choose where you would like to test
other installations. _(TODO: We need to list some benefits of each choice here.)_

1. [ ] Manager: Add new team member to the [dev-resources](https://gitlab.com/gitlab-com/dev-resources) project.
1. [ ] Use terraform to create a new test instance.
    1. [ ] Clone the dev-resources project.
    1. [ ] Read up on [how to create a new instance](https://gitlab.com/gitlab-com/dev-resources/blob/master/dev-resources/README.md).
    1. [ ] Create a new file and name it `<your-full-name>.tf` in a new branch. You
       can copy another team member's file and modify it or you can create your own
       from scratch. If you have any issues or questions ask in the `#support_self-managed` channel.
    1. [ ] After you've created it, open up a merge request and if the tests pass then merge it.
1. [ ] Set up your [test environments](https://about.gitlab.com/handbook/support/workflows/test_env.html).
1. [ ] Choose between Local VM's or Digital Ocean for your preferred test environment,
   and note it in a comment below.
1. [ ] Perform each of the following [Installation Methods](https://about.gitlab.com/installation/)
   on the test environment you chose above:
   1. [ ] Install via [Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab/)
   1. [ ] Populate with some test data: User account, Project, Issue
        - [Utilize the GitLab API to seed your test instance](https://gitlab.com/gitlab-com/support/support-training/blob/master/seed-data-api.md)
   1. [ ] Backup using our [Backup rake task](http://docs.gitlab.com/ee/raketasks/backup_restore.html#create-a-backup-of-the-gitlab-system)
   1. [ ] Install via [Docker](https://docs.gitlab.com/ee/install/docker.html) on your local machine.
   1. [ ] Restore a backup to your Docker VM using our [Restore rake task](http://docs.gitlab.com/ee/raketasks/backup_restore.html#restore-a-previously-created-backup)
   1. [ ] Install GitLab from [Source](https://docs.gitlab.com/ee/install/installation.html).
      Installation from source is not common but will give you a greater understanding
      of the components that we employ and how everything fits together.
        - [Have a copy of GitLab source code available offline for reference](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1900#2-have-a-copy-of-gitlab-source-code-available-offline-for-reference)

1. [ ] With your manager or another Support team member, manually generate 3 'self managed' licenses at https://license.gitlab.com/ one for each of Starter, Premium and Ultimate. Suggest around 100 users with no expiry. Keep the licenses securely in 1Password. Use the licenses on your test instance when replicating customer issues. By having the correct license you will ensure feature parity with what the customer is seeing.
1. The services below are part of the many services used by our customers. Installing these services and learning how they work with GitLab will prepare you for Stage 3 and give you a solid foundation for when you're ready to resolve tickets on your own.
    1. [ ] [GitLab Runner](https://docs.gitlab.com/runner/)
    1. [ ] [Elastic Search](https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html)
        1. [ ] Install on a VM or on a separate instance on Terraform
        1. [ ] Make sure you [install the JVM](https://www.elastic.co/guide/en/elasticsearch/reference/current/setup.html#jvm-version)
            * The [steps outlined on Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-elasticsearch-on-ubuntu-16-04) should help you configure your instance's IP
        1. [ ] [Connect Elastic Search to your GitLab instance and index your repositories](https://docs.gitlab.com/ee/integration/elasticsearch.html)
    1. [ ] [Kubernetes](https://docs.gitlab.com/ee/user/project/clusters/)
        1. [ ] Login to https://console.cloud.google.com/ with your work email
        1. [ ] Create a cluster under the project `gitlab-internal`. Remember to remove it when you're done!
        1. [ ] You will also need to install the [`gcloud SDK`](https://cloud.google.com/sdk/gcloud/reference/init)
        1. [ ] Create a project on your self-managed instance, and then create a file named `.gitlab-ci.yml`:
        1. [ ] Use [the yaml docs](https://docs.gitlab.com/ee/ci/yaml/#parameter-details) as a resource to build a basic script
        1. [ ] Make sure your project has access to your Kubernetes cluster under Operations -> Kubernetes
        1. [ ] Make sure your project has registered runners under your project's Settings -> CI/CD -> Runners
        1. [ ] Pat yourself on the back; you're now ready to reproduce some of our common customer tickets

1. [ ] You can check this box but this one never stops as long as you are a Support
      Engineer for GitLab: Keep this installation up-to-date as patch and version
      releases become available, just like our customers would.
1. [ ] Ask as many questions as you can think of in the `#support_self-managed` chat channel

### Stage 3. Customer Interaction through Zendesk

- [ ] **Done with Stage 3**

_Typically started in the first week, and completed by the end of the fourth week_

**Goals**

- Have a good understanding of ticket flow in Zendesk and how to interact with our various channels.
- See some common issues that our customers face and how to resolve them.

**Initial Zendesk training**

Zendesk is our Support Center and our main communication line with our customers.
We communicate with customers through several other channels too, see the support
handbook for the full list. Set aside around 40 minutes for completion of this section.

1. [ ] Complete Zendesk Agent training
   1. [ ] Navigate to [Zendesk university](http://training.zendesk.com/checkout/3djt0nv8i5y5r) and register.
      You'll receive an email with information on accessing the Zendesk course
   1. [ ] Complete the **"Zendesk Suite Overview: Support"** course (approx. 10 min)
1. [ ] Review additional Zendesk resources
   1. [ ] [UI Overview](https://support.zendesk.com/hc/en-us/articles/203661806-Introduction-to-the-Zendesk-agent-interface)
   1. [ ] [Updating Tickets](https://support.zendesk.com/hc/en-us/articles/212530318-Updating-and-solving-tickets)
   1. [ ] [Working w/ Tickets](https://support.zendesk.com/hc/en-us/articles/203690856-Working-with-tickets)
      *(Read [Avoiding Agent Collisions](https://support.zendesk.com/hc/en-us/articles/203690856-Working-with-tickets#topic_ryy_42g_vt) carefully).*
   1. [ ] [Using Macros](https://support.zendesk.com/hc/en-us/articles/203690796-Using-macros-to-update-tickets) (*Try the `Self-Managed > Pre customer call` in one of the tickets*)
   1. [ ] [Language Translation using Unbabel](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1805)

**Learn about the Support process**

1. [ ] Perform 15 one-hour pairing sessions with Support Engineers. Contact each in
   Slack to find a time that works, then create a Calendar event, inviting that person.
   When it's time for the pairing session, create a new [Support Pairing project Issue](https://gitlab.com/gitlab-com/support/support-pairing)
   and use the **Ticket Pairing** template for the call. Have at least 3 calls with a Support GitLab.com member.
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
1. [ ] Dive into our ZenDesk support process by reading how to [handle tickets](https://about.gitlab.com/handbook/support/onboarding/#handling-tickets).
1. [ ] Learn about our [Support Modes of Work](https://about.gitlab.com/handbook/support/workflows/support-modes-of-work.html#first-response-time-hawk).
   - Here you will learn about our four rotating roles so that people know what to do and have variation in their work.
1. [ ] Learn about the [hot queue](https://about.gitlab.com/handbook/support/workflows/working-with-tickets.html#hot-queue-how-gitlab-manages-ticket-assignments).
1. [ ] Learn about the [Zendesk SLA settings and Breach Alerts](https://about.gitlab.com/handbook/support/workflows/working-with-tickets.html#zendesk-sla-settings-and-breach-alerts).
1. [ ] Start getting real world experience by handling real tickets, all the while gaining further experience with GitLab.
   1. [ ] First, learn about our [Support Channels](https://about.gitlab.com/handbook/support/channels).
   1. [ ] Proceed on to [Regular email support tickets](https://about.gitlab.com/handbook/support/channels/#regular-zendesk-tickets).
      - Here you will find tickets from our GitLab EE Customers and GitLab CE Users
      - Tickets here are extremely varied and often very technical
      - You should be prepared for these tickets, given the knowledge gained from the previous steps of your training
      - Tickets in Zendesk contain a wealth of knowledge from past interactions. Often times, the issue has occurred at least once and a solution has been provided, or an issue created on the customer's behalf. Reading through past tickets can aide in the learning process, getting you up to speed quicker.
        - Look for errors, keywords, etc. to search Zendesk like so: `"keyword or error"`.
    1. [ ] [Zendesk Support search reference](https://support.zendesk.com/hc/en-us/articles/203663226-Zendesk-Support-search-reference)
1. [ ] Check out your colleagues' responses.
   1. [ ] Hop on to the `#zd-self-managed-feed` channel in Slack and see the tickets as they
      come in and are updated.
   1. [ ] Read through about 20 old tickets that your colleagues have worked on and their responses.
1. [ ] Watch the [GitLab Debugging Techniques: A Support Engineering Perspective](https://www.youtube.com/watch?v=9W6QnpYewik) video on GitLab Unfiltered (YouTube). This dives into some common issues that customers might run into.

**Learn about the Escalation process for Issues**

Some tickets need specific knowledge or a deep understanding of a particular component
and will need to be escalated to a Senior Support Engineer or a Developer.

1. [ ] Read about [creating issues](https://about.gitlab.com/handbook/support/onboarding/#create-issues)
   and [issue prioritization](https://about.gitlab.com/handbook/support/workflows/setting_ticket_priority.html).
1. [ ] Take a look at the [GitLab.com Team page](https://about.gitlab.com/team/)
   to find the resident experts in their fields.

**Learn about raising issues and fielding feature proposals**

1. [ ] Understand what's in the pipeline at GitLab as well as proposed features: [Direction Page](https://about.gitlab.com/direction/).
1. [ ] Practice searching issues and filtering using [labels](https://gitlab.com/groups/gitlab-org/-/labels)
   to find existing feature proposals and bugs.
1. [ ] When you're raising a new issue: always provide a relevant label, a link to the relevant ticket in Zendesk and the [SalesForce account link](https://about.gitlab.com/handbook/customer-success/tam/customer-issue-interest/#gitlab-issues-and-periscope).
1. [ ] Add [customer labels](https://about.gitlab.com/handbook/support/workflows/issue_escalations.html#adding-labels)
   for those issues relevant to our subscribers.
1. [ ] Take a look at the [existing issue templates](https://gitlab.com/gitlab-org/gitlab/tree/master/.gitlab/issue_templates)
   to see what is expected (look at the comments in the markup for details.)
1. [ ] Raise issues for bugs in a manner that would make the issue easily reproducible.
   A Developer or a contributor may work on your issue.
1. [ ] Schedule a 45 minute call with your trainer where you share your screen
   with them while you answer tickets on Zendesk, and they give you feedback and answer
   your questions. The goal of this call is for your trainer to pass on tactical
   knowledge about the ticket handling process. Repeat this step a few times if it would be beneficial to you.

#### Congratulations. You now have your Zendesk Wings!

From now on you can spend most of your work time answering tickets in Zendesk. Try
to set aside 2 hours per day to make it through **Stage 4-6** of this list. Never
hesitate to ask as many questions as you can think of in the `#support_self-managed` chat channel.

### Stage 4. Customer Calls

- [ ] **Done with Stage 4**

_Typically started in week 2 or 3, and completed by the end of week 4._

**Goal** Gain experience in scheduling and participating in calls with customers.

Look at the `GitLab Support` Google Calendar to find customer calls you can listen
in on. Contact the person leading the call to check if it is okay for you to jump
in on the call, and if they could stay on with you for a few minutes after the call,
so you can ask them a few questions about the things you didn't understand. Also, ask
them to ask you a few questions to make sure you understand the points they want to
highlight from the call.

1. [ ] Start arranging to pair on calls with other Support Engineers. Aim to cover
   two of each type of call. Comment on this issue with the type of call you were
   in, who it was with, and the link to the relevant ticket (if one exists).
   1. [ ] Learn about [Cisco WebEx](https://about.gitlab.com/handbook/support/onboarding/#webex).
     1. [ ] call with ___
     1. [ ] call with ___
     1. [ ] call with ___
   1. [ ] Reverse Shadowing _(**You** lead calls with a GitLab Support Engineer giving you feedback and support)_
     1. [ ] call with ___
     1. [ ] call with ___
     1. [ ] call with ___
     1. [ ] call with ___

1. [ ] Manager: invite the new team member to the group Calendly account (Account->Users->New User)
1. [ ] Read our [Calendly page](https://about.gitlab.com/handbook/support/workflows/calendly.html), and change it from the GitLab setup to how Support use it, including:
    1. [ ] Set up Zoom integration on your Calendly account by going to [calendly integrations](https://calendly.com/integrations) and following the instructions for Zoom (make sure to [update the calendly event's location to use "Zoom"](https://help.calendly.com/hc/en-us/articles/360010008093-Zoom))
    1. [ ] Set the title of your calendly events to include 'support' so the zap works, and install the browser plugin.
1. [ ] Calculate your desired work hours in UTC time and ping @dstanley to add you to the calendly Team Support Call event with that info.
1. [ ] Read [Customer Call process documentation](https://about.gitlab.com/handbook/support/workflows/customer_calls.html)

### Stage 5. Gathering Diagnostics

- [ ] **Done with Stage 5**

_Typically done around the third week._

**Goal** Understand the gathering of diagnostics for GitLab instances

1. [ ] Learn about the GitLab checks that are available
    1. [ ] [Environment Information and maintenance checks](https://docs.gitlab.com/ee/administration/raketasks/maintenance.html)
    1. [ ] [GitLab check](https://docs.gitlab.com/ee/administration/raketasks/check.html)
    1. [ ] Omnibus commands
        1. [ ] [Status](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#get-service-status)
        1. [ ] [Starting and stopping services](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#starting-and-stopping)
        1. [ ] [Starting a rails console](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#starting-a-rails-console-session)


1. [ ] Learn about additional Support Tools
    1. [ ] [GitLab SOS](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos)
        1. [SOS Analyzer](https://gitlab.com/gitlab-com/support/toolbox/sos-analyzer)
        1. [SOS For K8S](https://gitlab.com/gitlab-com/support/toolbox/kubesos)
    1. [ ] [Fast-Stats](https://gitlab.com/gitlab-com/support/toolbox/fast-stats)
    
  

### Stage 6. On Call Duty

- [ ] **Done with Stage 6**

_Typically to be completed within 8 weeks of joining._

**Goal** Aim to become a fully-fledged Support Engineer!

1. [ ] Read about on-call duty:
   1. [ ] GitLab's [on-call guide](https://about.gitlab.com/handbook/on-call/). On that
   page, the [Customer Emergency On-Call Rotation Section](https://about.gitlab.com/handbook/on-call/#customer-emergency-on-call-rotation)
   is what applies to Support Engineers, **NOT** the Production Team section.
   1. [ ] [GitLab Support's On-Call Guide](https://about.gitlab.com/handbook/support/on-call/) contains even more information specific to handling emergencies in Support.
1. [ ] Schedule time with your trainer to have them show you how to respond to Customer Emergencies.
1. [ ] Ping your manager to add you to PagerDuty.
   1. [ ] Manager: add the Engineer to the rotation with at least 6 weeks of lead time before their first on-call shift.
1. [ ] Sign up on PagerDuty with the link that will be emailed to you, and install the app on your phone.
   1. [ ] Familiarize yourself with the interface and the functionality.
1. [ ] Configure your personal notification rules in PagerDuty under "My Profile" > "Notification Rules"
   1. [ ] Currently, [customer emergency escalation policy](https://gitlab.pagerduty.com/escalation_policies#PKV6GCH) is set to 10 minutes.
      That means if you do not respond to the notification within this period, the emergency will escalate to the rest of the team.
      Make sure your personal notification rules take this into account.
   1. [ ] Remember to update this accordingly when your details changed
1. [ ] Use [this PagerDuty guide](https://support.pagerduty.com/docs/schedules-in-apps#section-export-only-your-on-call-shifts) to subscribe to your on-call schedule.
1. [ ] In the weeks leading up to your first on-call shift, shadow the on-call engineers who are handling customer emergencies.
   1. [ ] ZD Ticket: ___
   1. [ ] Repeat this as many times as you are able.

1. [ ] After your first on-call shift, add the "On-call champion!" moniker to your entry on the team page and assign the MR to your manager.


### Stage 7. Make the path better for those who come after.
You probably noticed that there was something broken or non-ideal in the onboarding process.
You might have brought it up to your manager.
They likely told you to look at Stage 7 of your onboarding issue.

1. [ ] Make an improvement to the Support Engineer Bootcamp issue.

### Stage 8. Optional Advanced GitLab Topics

Discuss with your training manager if you should stop here and close your issue
or continue. Also, discuss which of the advanced topics should be followed. Do
not do all of them as they might not be relevant to what customers need
right now and can be a significant time investment.

These are some of GitLab's more advanced features. You can make use of
GitLab.com to understand the features from an end-user perspective and then use
your own instance to understand setup and configuration of the feature from an
Administrative perspective

- [ ] Set up and try [Git LFS](https://docs.gitlab.com/ee/workflow/lfs/manage_large_binaries_with_git_lfs.html)
- [ ] Get to know the [GitLab API](https://docs.gitlab.com/ee/api/README.html), its capabilities and shortcomings
- [ ] Learn how to [migrate from SVN to Git](https://docs.gitlab.com/ee/workflow/importing/migrating_from_svn.html)
- [ ] Set up [GitLab CI](https://docs.gitlab.com/ee/ci/quick_start/README.html)
- [ ] Create your first [GitLab Page](https://docs.gitlab.com/ee/pages/administration.html)
- [ ] Learn about using Sentry to track errors:
    - [ ] [Sentry, GitLab & You](https://docs.google.com/presentation/d/1j1J4NhGQEYBY8la6lCK-N-bw749TbcTSFTD-ANHiels/edit#slide=id.p)
    - [ ] [Searching Sentry](https://about.gitlab.com/handbook/support/workflows/500_errors.html#searching-sentry)
- [ ] Learn about using Kibana to track errors:
    - [ ] [Kibana Training](https://about.gitlab.com/handbook/support/workflows/500_errors.html#searching-sentry)
    - [ ] [Searching Kibana](https://about.gitlab.com/handbook/support/workflows/500_errors.html#searching-kibana)
- [ ] [Advanced Gitlab debugging and log collection](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1900#5-try-something-a-little-more-advanced)


/label ~onboarding

/epic &16
