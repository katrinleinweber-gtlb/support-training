<!--

**Title:** *"GitLab.com CMOC Bootcamp - **your-name**"*

-->

**Goal of this bootcamp:** Instruct the taker on the duties and responsibilities of being the [Communications Manager On Call (CMOC)](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#roles-and-responsibilities) for an active GitLab.com incident. This includes providing a clear understanding of what an incident is, how to work with production engineering during one, and how to use the tools at our disposal to effectively communicate updates to incidents both internally and externally to users and customers.

Tackle each stage in sequential order.

## Stage 1: Commit to learning how to become a CMOC

+ [ ] Done with Stage 1
+ [ ] Ping your manager on this issue to notify them you have started.

## Stage 2: What is an Incident?

+ [ ] Done with Stage 2

### Reading

+ [ ] Read our [incident management page](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/) in its entirety with special attention to:
    + [ ] Read about [what an incident is](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#incidents).
    + [ ] Read about [the roles](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#roles-and-responsibilities) that are assumed during an incident, paying special attention to `CMOC`.

### Bookmarking & Slack

+ [ ] Bookmark the [production issue tracker](https://gitlab.com/gitlab-com/gl-infra/production/issues) and set yourself up to receive notifications for new, reopened, and closed issues in it.
+ [ ] Bookmark the [CMOC Handover issue tracker](https://gitlab.com/gitlab-com/support/dotcom/cmoc-handover/issues) so that you have quick access to the project used to handover to the next CMOC.
+ [ ] Join the [#production](https://gitlab.slack.com/messages/C101F3796), [#incident-management](https://gitlab.slack.com/messages/CB7P5CJS1), [#alerts](https://gitlab.slack.com/messages/C12RCNXK5), [#alerts-general](https://gitlab.slack.com/messages/CD6HFD1L0), [#announcements](https://gitlab.slack.com/archives/C8PKBH3M5), and [#dev-escalation](https://gitlab.slack.com/archives/CLKLMSUR4) Slack channels and consider [starring them](https://slack.com/help/articles/201331016-Star-channels-messages-or-files). You can mute the noisier alert based channels.

## Stage 3: Preparing for an Incident

+ [ ] Done with Stage 3

More often than not an incident will be discovered due to alert(s) received by production engineering. However, this will not always be the case. As you work, it's important to be mindful of patterns emerging in your tickets as you answer them.

+ [ ] Read and understand [how to determine if GitLab.com is experiencing an incident](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#is-gitlabcom-experiencing-an-incident).

Reports of problems with GitLab.com will primarily come from end-users through Zendesk but they may come from other sources as well. Bookmark the following community support resources and be prepared to check them for signs of issues with GitLab.com.

+ [ ] [Support Forum Issue Tracker](https://gitlab.com/gitlab-com/support-forum/issues)
+ [ ] [GitLab Community Forum](https://forum.gitlab.com/)

### Paging the EOC

More often than not we will learn about the existence of an incident from production engineering themselves. However, there may be times where our built-in alerting has not notified them of a problem that will turn into an incident. In these cases, once we've received enough reports about an issue that we believe to be on our end or we have sufficient reason in general to contact production engineering, we can page the EOC.

As a general rule, at least **three** separate reports of the same type of issue within a relatively short timeframe is a strong indicator that GitLab.com may be facing an incident and a page to the EOC is justified. When providing information to the EOC be sure to include links to Zendesk tickets and any other information they may need to understand the issue in a clear and concise manner.

+ [ ] Read about how to [page the EOC](https://about.gitlab.com/handbook/engineering/infrastructure/production/#sla-for-paging-on-call-team-members).

The EOC and IMOC for the production engineering team can be seen at any time by running the following Chatops command either in #production or in a [private message](https://gitlab.slack.com/messages/DK2BCPSNR) with GitLab Chatops.

```plain
/chatops run oncall prod
```

## Stage 4: Managing Incidents - Status.io

+ [ ] Done with Stage 4

Once an incident has [been declared](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#declaring-an-incident) the EOC will page the CMOC via PagerDuty and it'll be up to you to start managing Status.io. Along with this, one of your first tasks should be to join **The Situation Room** Zoom call so that you can follow along with the EOC and anyone else involved in working the incident.

+ [ ] Read about [how to conduct yourself while in The Situation Room](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#the-situation-room).

Effective communication with production engineering during an incident is crucial as the content of our status updates will largely come from them. Keeping yourself informed on the progress of an incident will allow you to communicate updates quickly and concisely with stakeholders and affected users.

Incidents and maintenance events are managed by the CMOC throughout their entire lifecycle through our [status page](https://status.gitlab.com/), powered by [Status.io](https://status.io). Updates made to incidents and maintenance events through Status.io are automatically tweeted out via [@gitlabstatus](https://twitter.com/gitlabstatus) through the [broadcast feature](https://kb.status.io/notifications/twitter-notifications/) of Status.io.

+ [ ] Learn how to [Create, Update, and Resolve](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#managing-incidents) incidents in Status.io.
+ [ ] Read about [Frequency of Updates](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#frequency-of-updates) to learn how often you should aim to update Status.io depending on the severity of the incident.
+ [ ] Learn how to [Perform a Handover](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#handover-procedure) at the end of your on-call shift.

## Stage 5: Review

+ [ ] Done with Stage 5
+ [ ] Review the following four past incidents to get an idea of the proper tone to use and what details to include when posting status updates.

1. [GitLab.com - elevated errors for git related to 1 node with network issues](https://status.gitlab.com/pages/incident/5b36dc6502d06804c08349f7/5da63402d8a2fe1a65de9a32)
1. [High latency for CI runners](https://status.gitlab.com/pages/incident/5b36dc6502d06804c08349f7/5da5a99706adb71d864dd1a7)
1. [Job queue takes longer to process jobs](https://status.gitlab.com/pages/incident/5b36dc6502d06804c08349f7/5d41a97276d55d04c63ed68a)
1. [We are looking into the increase of number of pending jobs](https://status.gitlab.com/pages/incident/5b36dc6502d06804c08349f7/5d1ccfa4fe44802c4ada74e4)

If you need to review more examples, browse the [Incident History](https://app.status.io/dashboard/5b36dc6502d06804c08349f7/incidents) section of Status.io.

## Completion

+ [ ] Let your manager know that you've completed this bootcamp and you need access to Status.io by filling out an access request and pinging them.

/assign me
/label ~bootcamp
