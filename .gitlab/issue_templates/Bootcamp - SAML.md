**Title:** _"SAML Boot Camp - **your-name**"_

**Goal of this checklist:** Set a clear path for SAML Expert training

**Objectives**: At the end of this bootcamp, you should be able to:
* Understand how GitLab leverages the OmniAuth gem with a SAML Strategy to act as a SAML 2.0 Service Provider
* Troubleshoot customer's issues with SAML

Remember to contribute to any documentation that needs updating

### Stage 1: Commit and Become familiar with what SAML is

- [ ] **Done with Stage 1**

1. [ ] Ping your manager on the issue to notify them you have started
1. [ ] Commit to this by notifying the current experts that they can start
routing less-technical SAML to you
1. [ ] Read through the [GitLab SAML Documentation](https://docs.gitlab.com/ee/integration/saml.html)
1. [ ] Read through the [GitLab SAML SSO for Groups Documentation](https://docs.gitlab.com/ee/user/group/saml_sso/)
1. [ ] Read throught the [omniauth-saml gem documentation](https://github.com/omniauth/omniauth-saml)
1. [ ] Watch the [Manage 201 SAML knowledge sharing](https://www.youtube.com/watch?v=CW0SujsABrs), you can access the [slides](https://docs.google.com/presentation/d/10uUGoDB4nN0Ho42vwq3-aTO5UMO5a3Bjgor8maWe-zw/edit#slide=id.g444cb56ba0_0_0) as well

### Stage 2: Technical Setup

- [ ] **Done with Stage 2**

1. Implement SAML on the GDK

   1. [ ] Using the [SAML How To Documentation](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/saml.md), set up a SAML IdP using the provided Docker image.
   1. [ ] Set up both instance-wide SAML and Group SAML on your GDK instance.
   1. [ ] Contribute to the documentation with any issues or troubleshooting steps.

### Stage 3: Tickets

- [ ] **Done with Stage 3**

1. [ ] Go through 10 solved SAML tickets to check the responses and get a sense
of the types of frequently asked questions that come up.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Answer 10 SAML tickets and paste the links here, even if a ticket seems
too advanced for you to answer. Find the answers from an expert and relay them to
the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

### Stage 4: Pair on Customer Calls

- [ ] **Done with Stage 4**

1. [ ] Pair on two Diagnostic calls, where a customer has a problem with SAML.
   1. [ ] call with ___
   1. [ ] call with ___

### Penultimate Stage: Review
You feel that you can now do all of the objectives:
1. [ ] Understand how GitLab leverages the OmniAuth gem with a SAML Strategy to act as a SAML 2.0 Service Provider
2. [ ] Troubleshoot customer's issues with SAML

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this bootcamp or in other documentation, list it below as tasks for yourself!
* [ ] Update ...

### Final Stage

- [ ] Have your trainer and manager review this issue.
- [ ] Manager: schedule a call (or integrate into 1:1) to review how the bootcamp went once you have reviewed this issue.
- [ ] Send a MR to declare yourself a **SAML Expert** on the team page

/epic &16
