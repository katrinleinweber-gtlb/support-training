Title: "Omnibus Bootcamp - your-name"

## Goal of this checklist

> Set a clear path for Omnibus Expert training

## Objectives:

* Know how to install the latest version GitLab via Omnibus.
* Know how to install a specific version of GitLab via Omnibus.
* Know how to both upgrade and downgrade an Omnibus install of GitLab.
* Know how to both backup and restore an Omnibus install of GitLab.
* Know how to reset a user's password via the gitlab-rails console.
* Know how to apply a patch to an Omnibus install of GitLab.
* Be able to locate the logs that detail the previous reconfigures that were
  run.

---

### Stage 1: Commit and become familiar with what Omnibus is

- [ ] **Done with Stage 1**

   1. [ ] Ping your manager on the issue to notify them you have started.
   1. [ ] In your Slack Notification Settings, set **Omnibus** and **Upgrade** as
          Highlight Words
   1. [ ] Commit to this by notifying the current experts that they can start
          routing non-technical Omnibus questions to you
   1. [ ] Learn about Omnibus:
      1. [ ] Read through [Omnibus GitLab Docs](https://docs.gitlab.com/omnibus/)
      1. [ ] Familiarize yourself with [GitLab Installation Methods](https://about.gitlab.com/install/)
      1. [ ] Watch Alex's video about the [GitLab Omnibus](https://youtu.be/dqImFA11dtk)
      1. [ ] Read through [Manually Downloading and Installing a GitLab Package](https://docs.gitlab.com/omnibus/manual_install.html)
      1. [ ] Read through [Backing up and restoring GitLab](https://docs.gitlab.com/ee/raketasks/backup_restore.html)
      1. [ ] Read through [Updating GitLab installed with the Omnibus GitLab package](https://docs.gitlab.com/omnibus/update/)
      1. [ ] Read through [Upgrade recommendations](https://docs.gitlab.com/ee/policy/maintenance.html#upgrade-recommendations)
      1. [ ] Read through [Downgrading](https://docs.gitlab.com/omnibus/update/README.html#downgrading)
      1. [ ] Read through [Upgrade packaged PostgreSQL server](https://docs.gitlab.com/omnibus/settings/database.html#upgrade-packaged-postgresql-server)
      1. [ ] Read through [Patching an instance](https://about.gitlab.com/handbook/support/workflows/patching_an_instance.html)

### Stage 2: Technical setup

- [ ] **Done with Stage 2**

    Review [setup-omnibus-bootcamp](https://gitlab.com/astrachan-setups/setup-omnibus-bootcamp) and consider using this to provision your VM and to capture any progress notes.
   1. [ ] Follow [GitLab Installation Methods](https://about.gitlab.com/install/)
          to install the latest version of GitLab on a VM.
   1. [ ] Follow [Manually Downloading and Installing a GitLab Package](https://docs.gitlab.com/omnibus/manual_install.html)
          to install GitLab version 11.3.4 on a VM.
   1. [ ] Successfully create a backup of a running GitLab instance including configuration files.
   1. [ ] Perform a restore using the backup you created.
   1. [ ] Practice upgrading and downgrading your instance.

### Stage 3: Quiz

- [ ] **Done with Stage 3**

   1. [ ] Schedule a call with a current Omnibus Master. During this call, you
          will guide them through the following:
      1. [ ] Install GitLab version 11.9.9 via Omnibus.
      1. [ ] Upgrading the GitLab installation to version 12.0.0, telling it to
             skip the forced postgres upgrade. Make sure to follow best practices!
      1. [ ] Downgrade the GitLab version to version 11.11.8.
      1. [ ] Performing a restore on the GitLab instance using `1573750000_2019_11_14_11.11.8-ee_gitlab_backup.tar`
             and `gitlab-secrets.json` from [bootcamp-omnibus - content](https://gitlab.com/gitlab-com/support/support-training/tree/master/content/bootcamp-omnibus)
      1. [ ] Apply `example.patch` from [bootcamp-omnibus - content](https://gitlab.com/gitlab-com/support/support-training/tree/master/content/bootcamp-omnibus) 
             to your instance.
      - **Note:** Be sure to reference or show any links that you looked up at each
        step of the quiz
   1. [ ] Once you have completed this, have the master comment below
          acknowledging your success. 

### Final Stage:

   1. [ ] Your Manager needs to check this box to acknowledge that you finished
   1. [ ] Send a MR to declare yourself an Omnibus Expert on the team page

/label bootcamp
/epic &16
