_We need to keep iterating on this checklist so please submit MR's for any improvements
that you can think of. The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/Onboarding%20-%20GitLab.com%20Support%20Agent.md).
If an item does not start with a role or someone else's name, it's yours to do._

**Goal of this entire checklist:** Set a clear path for GitLab.com Support Agent training

## Manager Tasks pre-Stage 0

- [ ] Open Support Engineering onboarding issue
- [ ] Open [Support Engineering access request issue](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Support_Engineer)
- [ ] Add new team member to:
    - regional call
    - DOTCOM meeting
- [ ] Schedule 1:1 meeting
    - [ ] Create 1:1 doc
    - [ ] Schedule 1st day
- [ ] Add new team member to calendars:
    - [ ] GitLab Support
    - [ ] Support Time Off
- [ ] Verify new team member has updated BambooHR with their primary and other citizenships
   - [ ] Verify if new team member is a US citizen living in the US
   - [ ] (If they are) add them to the Federal Zendesk instance as Staff
- [ ] Check Stage 0 for access request checklist, and other manager tasks.

## Your trainer: `[tag trainer]`

### Stage 0: Complete general onboarding to have all necessary accounts and permissions

- [ ] **Done with Stage 0**

_Typically completed within the first week_

> **As you work through this onboarding issue, please remember that this issue is [non-confidential and is public](https://about.gitlab.com/handbook/values/#transparency). With that said, please do not add any confidential data such as customer names, logs, etc. to this issue. Linking to the appropriate [Zendesk ticket](https://about.gitlab.com/handbook/support/workflows/working_with_security.html#general-guidelines-1) is OK!**

1. [ ] General expectations: it is expected that you will _start_ to tackle Stages
   0, 1, 2, and 3 as early as your first week, but you are not expected to complete
   all items for a number of weeks. We believe that you often learn best and fastest
   by diving into (relatively easy) tickets, and learning by doing. However, this
   has to be balanced with making time to learn some of the tougher topics. The expectation
   is therefore that you are sufficiently advanced in your onboarding by the end of
   your first week that you can answer 2-5 "simple" tickets. Over the following weeks,
   your manager will set higher targets for the number and difficulty of tickets to
   be tackled, but you should always save about 2-3 hrs per day to spend on working
   through this bootcamp. Some days it will be less, some days it will be more, depending
   on ticket load and how deep "in the zone" you are in either the bootcamp or the tickets
   you are responding to; but an average of 2-3 hrs per day should allow you to complete
   everything through to the end of Stage 4 within about four weeks.
1. [ ] General Onboarding Checklist: this should have been created for you on [the People Ops Employment issue tracker](https://gitlab.com/gitlab-com/people-ops/employment/issues/)
   when you were hired.
   1. [ ] Finish every item on that list starting with `New team member:` until you
      are waiting for someone to answer a question or do something before you can continue that list.
   1. [ ] Ensure that as part of general onboarding, you and your manager check in that you have been added to all relevant Engineering communications channels. Access can be requested via a [Single Person Access Request](https://about.gitlab.com/handbook/business-ops/it-ops-team/access-requests/#single-person-access-request). Relevant channels include:
      - [ ] Email list (Google Groups)
        - engineering@gitlab.com
        - supportteam@gitlab.com
      - Google Drive shared drives (auto access via Google Groups)
        - Support
        - Engineering
      - [ ] 1password Support Vault
      - [ ] In ZenDesk as Staff in Support Group
      - [ ] Slack
        - Join `#support-team-chat`, `#support_gitlab-com`, `#support_self-managed`, `#support-managers`
        - Added to `@support-dotcom` and/or `@support-selfmanaged` group
        - Added to `@support-team-amer`, `@support-team-emea` or `@support-team-apac` group
      - [ ] GitLab groups and aliases:
        - [ ] _Manager:_ add team member to `gitlab-com/support/dotcom` as `Maintainer`
        - [ ] _Manager:_ add team member to `gitlab-com/support` as `Maintainer`
        - [ ] _Manager:_ add team member to regional group `gitlab-com/support/amer`, `gitlab-com/support/apac` or `gitlab-com/support/emea` as `Owner`
   1. [ ] Start Stage 1 here, as well as the first steps of Stage 2 and Stage 3.
   1. [ ] Check back daily on what was blocking you on your General Onboarding Checklist
      until that list is completely done, then focus on this one.
1. [ ] Team Calls: To keep up with the changes and happenings within the Support team, we have team calls every week. Every member of the support team is encouraged to join so they can also state their opinions during the call.
   1. [ ] Read up on the [calls the Support Team uses to sync up](https://about.gitlab.com/handbook/support/#weekly-meetings) and make sure you have the ones that pertain to you on your calendar.
   1. [ ] Identify agendas for those meetings, and read through a few past meetings in the document.
   1. [ ] Verify that you have a 1:1 scheduled with your manager and you have access to the agenda for that meeting.
1. [ ] Introduce yourself to your team!
   1. [ ] Write an entry with your name, location, the date you started, a quick blurb about your experience, personal interests and what drew you to GitLab support in the [Support Week in Review doc](https://docs.google.com/document/d/1eyMzbzImSKNFMpmu33C6imvC1iWEWHREJqaD6mkVDNg/edit) before Friday.
      1. [ ] _Manager:_ Make sure to introduce the team member in the [Engineering Week-in-Review document](https://docs.google.com/document/d/1Oglq0-rLbPFRNbqCDfHT0-Y3NkVEiHj6UukfYijHyUs/edit#) and copy the introduction over to [Support Week in Review](https://docs.google.com/document/d/1eyMzbzImSKNFMpmu33C6imvC1iWEWHREJqaD6mkVDNg/edit#heading=h.pykb7ntmpjic)!
      1. [ ] _Manager:_ Talk to the new team member about doing an introduction at the next regional team meeting, and add it to the agenda.
   1. [ ] Now, also format and send the introduction post you just created to the `#support-team-chat` Slack channel. Welcome to [multi-modal communication](https://about.gitlab.com/handbook/communication/#multimodal-communication), key to effective communication at GitLab!

### Stage 1: Become familiar with git and GitLab basics

- [ ] **Done with Stage 1**

_Typically started in the first week, completed by end of the second week_

If you are already comfortable with using Git and GitLab and you are able to
retain a good amount of information by just watching or reading through, that is
okay. But if you see a topic that is completely new to you, stop the video and try
it out for yourself before continuing.

1. [ ] Let your manager know you're ready to be assigned a trainer.
1. [ ] Add your **Primary Citizenship** and **Additional Citizenship(s)** to the fields in BambooHR if you haven't already.
   - [ ] Manager: if they are US Citizens based in the US, add them to the Federal ZD instance.
1. [ ] Just quickly check on your Zendesk account to make sure that is ready for you when you need it.
1. [ ] Add a [profile picture](https://support.zendesk.com/hc/en-us/articles/203690996-Updating-your-user-profile-and-password#topic_rgk_2z2_kh)
   to your Zendesk account
1. [ ] Let your manager know if you were not able to create an account in Zendesk for some reason.
1. [ ] Under your profile in Zendesk, it should read `Support Staff`. If it reads `Light Agent`, inform your manager.
1. [ ] Get familiar with what [support workflows](https://about.gitlab.com/handbook/support/workflows/#support_workflows) exist, especially the `Support Workflows` and `GitLab.com` sections.
1. [ ] [Request access to ChatOps](https://docs.gitlab.com/ee/development/chatops_on_gitlabcom.html#chatops-on-gitlabcom)
   1. [ ] Manager: grant access to ChatOps group per the above documentation

Go over these topics in [GitLab University](https://docs.gitlab.com/ee/university/):

1. [ ] Under the topic of [Version Control and Git](https://docs.gitlab.com/ee/university/#1-1-version-control-and-git)
   1. [ ] [About Version Control](https://docs.google.com/presentation/d/16sX7hUrCZyOFbpvnrAFrg6tVO5_yT98IgdAqOmXwBho/edit#slide=id.gd69537a19_0_14)
   1. [ ] [Try Git](https://www.codecademy.com/learn/learn-git)
1. [ ] Under the topic of [GitLab Basics](https://docs.gitlab.com/ee/university/#12-gitlab-basics)
   1. [ ] All the [GitLab Basics](http://docs.gitlab.com/ee/gitlab-basics/README.html)
      that you don't feel comfortable with. If you get stuck, see the linked videos
      under GitLab Basics in GitLab University
   1. [ ] [GitLab Flow](https://www.youtube.com/watch?v=UGotqAUACZA)
   1. [ ] Take a look at how the different GitLab versions [compare](https://about.gitlab.com/features/)
1. [ ] Any of these that you don't feel comfortable with in the [user training](https://gitlab.com/gitlab-org/gitlab-ee/tree/master/doc/university/training/topics)
   we use for our customers.
   1. [ ] `env_setup.md`
   1. [ ] `feature_branching.md`
   1. [ ] `explore_gitlab.md`
   1. [ ] `stash.md`
   1. [ ] `git_log.md`
   1. [ ] For the rest of the topics in `user training`, just do a quick read over
      the file names so you start remembering where to find them.
1. [ ] Get familiar with the services GitLab offers
   1. [ ] The differences between GitLab.com [Free, Bronze, Silver, Gold](https://about.gitlab.com/pricing/#gitlab-com)
      (which you'll be supporting)
   1. [ ] The differences between self-managed [Core, Starter, Premium, Ultimate](https://about.gitlab.com/pricing/#self-managed)
1. [ ] Get familiar with the [different teams in-charge of every stage in the DevOps cycle](https://about.gitlab.com/handbook/product/categories/#devops-stages) and what they are responsible for. This will assist you in adding the right labels when creating issues and escalating in the right Slack channel.

### Stage 2. Customer Interaction through Zendesk

- [ ] **Done with Stage 2**

_Typically started in the first week, and completed by the end of the fourth week_

**Goals**

- Have a good understanding of ticket flow in Zendesk and how to interact with our various channels.
- See some common issues that our customers face and how to resolve them.

**Initial Zendesk training**

Zendesk is our Support Center and our main communication line with our customers.
We communicate with customers through several other channels too, see the support
handbook for the full list. Set aside around 40 minutes for completion of this section.

1. [ ] Complete Zendesk Agent training
   1. [ ] Navigate to [Zendesk university](http://training.zendesk.com/checkout/3djt0nv8i5y5r) and register. You'll receive an email with information on accessing the Zendesk course
   1. [ ] Complete the **"Zendesk Suite Overview: Support"** course (approx. 10 min)
1. [ ] Review additional Zendesk resources
   1. [ ] [UI Overview](https://support.zendesk.com/hc/en-us/articles/203661806-Introduction-to-the-Zendesk-agent-interface)
   1. [ ] [Updating Tickets](https://support.zendesk.com/hc/en-us/articles/212530318-Updating-and-solving-tickets)
   1. [ ] [Working w/ Tickets](https://support.zendesk.com/hc/en-us/articles/203690856-Working-with-tickets)
      *(Read [Avoiding Agent Collisions](https://support.zendesk.com/hc/en-us/articles/203690856-Working-with-tickets#topic_ryy_42g_vt) carefully).*

**Learn about the Support process**

1. [ ] Perform 15 one-hour pairing sessions with Support Agents or Support Engineers. Focus on GitLab.com team members, but consider self-managed team members as well as people outside of your immediate region to avoid silos. Contact
   each in Slack to find a time that works, then create a Calendar event, inviting
   that person. When it's time for the pairing session, create a new [Support Pairing project Issue](https://gitlab.com/gitlab-com/support/support-pairing)
   and use the **.com Support Ticket Pairing** template for the call. At least five sessions should be where you share your screen while you answer tickets on Zendesk, and including at least one with your trainer, where they give you feedback and answer your questions. The goal of the call with the training for your trainer to pass on tactical knowledge about the ticket handling process. Repeat this step a few times if you find it useful.
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
1. [ ] Dive into our Zendesk support process by reading how to [handle tickets](https://about.gitlab.com/handbook/support/onboarding/#handling-tickets).
1. [ ] Learn about our [Support Modes of Work](https://about.gitlab.com/handbook/support/workflows/support-modes-of-work.html#first-response-time-hawk).
   - Here you will learn about our four rotating roles so that people know what to do and have variation in their work.
1. [ ] Learn about the [hot queue](https://about.gitlab.com/handbook/support/workflows/working-with-tickets.html#hot-queue-how-gitlab-manages-ticket-assignments).
1. [ ] Learn about the [Zendesk SLA settings and Breach Alerts](https://about.gitlab.com/handbook/support/workflows/working-with-tickets.html#zendesk-sla-settings-and-breach-alerts).
1. [ ] Read the [authentication and security best practices documentation](https://gitlab.com/gitlab-com/support/support-team-meta/tree/master/docs/dotcom/authentication).
1. [ ] Start getting real world experience by handling real tickets, all the while gaining further experience with GitLab.
   1. [ ] First, learn about our [Support Channels](https://about.gitlab.com/handbook/support/channels).
   1. [ ] Proceed on to [Regular email support tickets](https://about.gitlab.com/handbook/support/channels/#regular-zendesk-tickets).
      - Here you will find tickets from both our GitLab.com Customers (paid) and GitLab.com Users (free)
      - Tickets here are extremely varied
      - You should be prepared for these tickets, given the knowledge gained from the previous steps of your training
1. [ ] Check out your colleagues' responses.
   1. [ ] Hop on to the #zd-self-managed-feed channel in Slack and see the tickets as they
      come in and are updated. You can also join #zd-gitlab-com-feed to see tickets from the GitLab.com Support team specifically.
   1. [ ] Read through about 20 old tickets that your colleagues have worked on and their responses.
1. Get to know the GitLab [Support Bot](https://gitlab.com/gitlab-com/support/support-ops/gitlab-support-bot/tree/master).
   1. [ ] In the `#support_gitlab-com` Slack channel, type "sb s". This will return a list of the number of active tickets by type.
1. [ ] Typically at week 3-5, you and your manager should check in that you're ready for admin access. Submit a [Single Person Access Request](https://about.gitlab.com/handbook/business-ops/it-ops-team/access-requests/#single-person-access-request) for admin access.

**Learn about the Escalation process for Issues**

Some tickets need specific knowledge or a deep understanding of a particular component
and will need to be escalated to a Senior GitLab.com Support Agent or a Developer.

1. [ ] Read about [creating issues](https://about.gitlab.com/handbook/support/onboarding/#create-issues)
   and [issue prioritization](https://about.gitlab.com/handbook/support/workflows/issue_escalations.html#issue-prioritization).
1. [ ] Take a look at the [GitLab.com Team page](https://about.gitlab.com/team/)
   to find the resident experts in their fields.
1. [ ] Read about how [support engineers escalate issues](https://about.gitlab.com/handbook/support/workflows/issue_escalations.html).

**Learn about raising issues and fielding feature proposals**

1. [ ] Understand what's in the pipeline at GitLab as well as proposed features: [Direction Page](https://about.gitlab.com/direction/).
1. [ ] Practice searching issues and filtering using [labels](https://gitlab.com/groups/gitlab-org/-/labels)
   to find existing feature proposals and bugs.
1. [ ] If raising a new issue always provide a relevant label and a link to the relevant ticket in Zendesk.
1. [ ] Add [customer labels](https://about.gitlab.com/handbook/support/workflows/issue_escalations.html#adding-labels)
   for those issues relevant to our subscribers.
1. [ ] Take a look at the [existing issue templates](https://gitlab.com/gitlab-org/gitlab/tree/master/.gitlab/issue_templates)
   to see what is expected (look at the comments in the markup for details.)
1. [ ] Raise issues for bugs in a manner that would make the issue easily reproducible.
   A Developer or a contributor may work on your issue.
1. [ ] Update notification settings in the [dotcom-internal](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal) project to **Watch** which will send you notifications for all activity.
1. [ ] Update notification settings in the [GDPR](https://gitlab.com/gitlab-com/gdpr-request) project to **Watch** which will send you notifications for all activity.

Practice researching issues and responding via the [GitLab.com Support Tracker](https://about.gitlab.com/handbook/support/channels/#gitlabcom-support-tracker) and the [GitLab Community Forum](https://forum.gitlab.com/).
This is just during onboarding to help you get exposure to the types of problems that you may see. We as GitLab Support do not officially support these platforms, though you should still check them occasionally and assist free users when possible.

1. [ ] In the [GitLab.com Support Tracker](https://about.gitlab.com/handbook/support/channels/#gitlabcom-support-tracker), choose, research, respond (& escalate if applicable) 3 issues. List Issue #s below.
      1. [ ] _________
      1. [ ] _________
      1. [ ] _________
1. [ ] In [GitLab Community Forum](https://forum.gitlab.com/), choose, research, respond (& escalate if applicable) 3 posts. Remember, the Community Forum is for any GitLab question, not just exclusive to GitLab.com. List links to forum posts below.
      1. [ ] _________
      1. [ ] _________
      1. [ ] _________

#### Congratulations. You now have your Zendesk Wings!

From now on you can spend most of your work time answering tickets in Zendesk. Never
hesitate to ask as many questions as you can think of in the `#support_gitlab-com` chat channel.

### Stage 3. Feedback and Documentation

- [ ] **Done with Stage 3**

Keeping our documentation and workflows up to date will ensure that all GitLab.com Support
Agents will be able to access and learn from the best practices of the past. Giving
feedback about your onboarding experience will ensure that this document is always
up to date and those coming after you will have an easier time coming in.

1. [ ] Schedule a call with your manager to discuss your onboarding issue. Make a comment with answers to the following questions:
   - What things were helpful?
   - What things did you wish existed in your onboarding, but did not?
   - Which areas do you not feel confident in yet?
   - Which areas do you feel strong in?
   - Discuss which areas of documentation need to be updated or created. Then update / create three of them:
      1. [ ] _________
      1. [ ] _________
      1. [ ] _________

1. [ ] Make an update to this onboarding template to make it better.

### Stage 4. Optional Advanced GitLab Topics

Discuss with your training manager if you should stop here and close your issue
or continue. Also, discuss which advanced topics are required. Do
not do all of them as they might not be relevant to what customers need
right now and can be a significant time investment.

These are some of GitLab's more advanced features. You can make use of
GitLab.com to understand the features from an end-user perspective.

- [ ] Create your first [GitLab Page](https://docs.gitlab.com/ee/user/project/pages/)
- [ ] Set up [GitLab CI](https://docs.gitlab.com/ee/ci/quick_start/README.html)

Good to know:

- [ ] Set up and try [Git LFS](https://docs.gitlab.com/ee/workflow/lfs/manage_large_binaries_with_git_lfs.html)
- [ ] Get to know the [GitLab API](https://docs.gitlab.com/ee/api/README.html), its capabilities and shortcomings. It's recommended to use [Insomnia](https://insomnia.rest/) to experiment with API calls.
- [ ] Learn about using Sentry to track errors:
    - [ ] [Sentry, GitLab & You](https://docs.google.com/presentation/d/1j1J4NhGQEYBY8la6lCK-N-bw749TbcTSFTD-ANHiels/edit#slide=id.p)
    - [ ] [Searching Sentry](https://about.gitlab.com/handbook/support/workflows/500_errors.html#searching-sentry)
- [ ] Learn about using Kibana to track errors:
    - [ ] [Kibana Training](https://about.gitlab.com/handbook/support/workflows/500_errors.html#searching-sentry)
    - [ ] [Searching Kibana](https://about.gitlab.com/handbook/support/workflows/500_errors.html#searching-kibana)

Optional:

- [ ] Learn how to [migrate from SVN to Git](https://docs.gitlab.com/ee/user/project/import/svn.html)
- [ ] Get familiar with the GitLab source code by finding the differences
between the [EE codebase](https://gitlab.com/gitlab-org/gitlab-ee) and the [CE codebase](https://gitlab.com/gitlab-org/gitlab-ce)

/label ~onboarding

/epic &16
