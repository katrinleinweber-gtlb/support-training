Title: "Service Desk Bootcamp - your-name"

## Goal of this checklist

> Set a clear path for Service Desk Expert training

## Objectives:

* Learn about Service Desk
* Learn about GitLab's Service Desk integration
* Feel comfortable answering some common scenarios.

---

* ### Stage 1: Commit and become familiar with what Service Desk is
  * [ ] Ping your manager on the issue to notify them you have started.
  * [ ] In your Slack Notification Settings, set Service Desk and Incoming Email
        as Highlight Words
  * [ ] Commit to this by notifying the current experts that they can start
        routing non-technical Service Desk questions to you
  * [ ] Read documentation
    * [ ] [Service Desk](https://docs.gitlab.com/ee/user/project/service_desk.html)
    * [ ] [Setting Up Incoming Email](https://docs.gitlab.com/ee/administration/incoming_email.html#set-it-up)
    * [ ] [Using customized email templates](https://docs.gitlab.com/ee/user/project/service_desk.html#using-customized-email-templates)
    * [ ] [Service Desk Troubleshooting](https://about.gitlab.com/handbook/support/workflows/service_desk_troubleshooting.html)
  * [ ] Watch videos
    * [ ] [Demo of Service Desk](https://www.youtube.com/watch?v=m6oHRIeT1AE&feature=youtu.be)
    * [ ] [Learn about Service Desk](https://youtu.be/PId7jlcAkqk)
  * [ ] Ticket Review
    * [ ] Importance of `%{key}` in the config:
      * [ ] Ticket: [141916](https://gitlab.zendesk.com/agent/tickets/141916)
      * [ ] Code: [incoming_email.rb](https://gitlab.com/gitlab-org/gitlab/blob/v12.6.1-ee/lib/gitlab/incoming_email.rb#L10-20)
    * [ ] GitLab ignores auto-replies, which might catch automatically forwarded emails
      * [ ] Ticket: [129899](https://gitlab.zendesk.com/agent/tickets/129899)
      * [ ] Code: [receiver.rb](https://gitlab.com/gitlab-org/gitlab/blob/v12.6.1-ee/lib/gitlab/email/receiver.rb#L99-118)
    * [ ] Sometimes, we just need to get the customer to test sending an email
      * [ ] Ticket: [137369](https://gitlab.zendesk.com/agent/tickets/137369)
* ### Stage 2: Technical setup
  * [ ] Setup a GitLab instance via Omnibus (you will need a premium level
        license)
  * [ ] Enable incoming email on your GitLab instance (recommended to use your
        `@gitlab.com` email account)
  * [ ] Check your email credentials are working (
        `sudo gitlab-rake gitlab:incoming_email:check`)
  * [ ] Create a new project named `service-desk-test`
  * [ ] Test Service Desk is working
    * Create an issue via email
    * Comment on the issue via the GitLab UI
    * Reply to said comment via email
  * [ ] Create a new `Thank you email` template 
* ### Stage 3: Quiz
  * [ ] Ping a master to assist you in testing your instance. The only thing you
        need to give the master is the email address to use for your project
        (created in Stage 2).
  * [ ] Request the master create an issue on the project via email.
  * [ ] Post a screenshot of the created issue with the master's comment down
        below.
  * [ ] Post a screenshot of the `Thank you email` (make sure it is using your 
        custom template) that the master recieves after they create an issue.
  * [ ] Once you have completed this, have the master comment below
        acknowledging your success.
* ### Final Stage:
  * [ ] Your Manager needs to check this box to acknowledge that you finished
  * [ ] Send a MR to declare yourself an Service Desk Expert on the team page

/label bootcamp
/epic &16
