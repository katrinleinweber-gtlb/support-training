<!-- Please use either the Support Agent or Suppor Engineer self-managed onboarding templates until this one is reviewed. -->

_We need to keep iterating on this checklist so please submit MR's for any improvements
that you can think of. The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/Onboarding%20-%20GitLab.com%20Support%20Engineer.md)
If an item does not start with a role or someone else's name, it's yours to do._

**Goal of this entire checklist:** Set a clear path for Support Engineer training

## Manager Tasks pre-Stage 0

- [ ] Open Support Engineering onboarding issue
- [ ] Open [Support Engineering access request issue](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Support_Engineer)
- [ ] Add new team member to:
    - regional call
    - DOTCOM meeting
- [ ] Schedule 1:1 meeting
    - [ ] Create 1:1 doc
    - [ ] Schedule 1st day
- [ ] Add new team member to calendars:
    - [ ] GitLab Support
    - [ ] Support Time Off
- [ ] Verify new team member has updated BambooHR with their primary and other citizenships
   - [ ] Verify if new team member is a US citizen living in the US
   - [ ] (If they are) add them to the Federal Zendesk instance as Staff
- [ ] Check Stage 0 for access request checklist, and other manager tasks.

## Your trainer: `[tag trainer]`

:warning: This issue is `Not confidential`, so everybody can see it. Do not mention sensitive or confidential personal, customer, or GitLab related information in this issue. That includes e.g. mentioning customer names for tickets that you worked on.

### Stage 0: Complete general onboarding to have all necessary accounts and permissions

- [ ] **Done with Stage 0**

_Typically completed within the first week_

1. [ ] General expectations: it is expected that you will _start_ to tackle Stages
   0, 1, 2, and 3 as early as your first week, but you are not expected to complete
   all items for a number of weeks. We believe that you often learn best and fastest
   by diving into (relatively easy) tickets, and learning by doing. However, this
   has to be balanced with making time to learn some of the tougher topics. The expectation
   is therefore that you are sufficiently advanced in your onboarding by the end of
   your first week that you can answer 2-5 "simple" tickets. Over the following weeks,
   your manager will set higher targets for the number and difficulty of tickets to
   be tackled, but you should always save about 2-3 hrs per day to spend on working
   through this bootcamp. Some days it will be less, some days it will be more, depending
   on ticket load and how deep "in the zone" you are in either the bootcamp or the tickets
   you are responding to; but an average of 2-3 hrs per day should allow you to complete
   everything through to the end of Stage 6 within about four weeks.
1. [ ] General Onboarding Checklist: this should have been created for you on [the People Ops Employment issue tracker](https://gitlab.com/gitlab-com/people-ops/employment/issues/)
   when you were hired.
   1. [ ] Finish every item on that list starting with `New team member:` until you
      are waiting for someone to answer a question or do something before you can continue that list.
   1. [ ] Ensure that as part of general onboarding, you and your manager check in that you have been added to all relevant Engineering communications channels. Access can be requested via a [Single Person Access Request](https://about.gitlab.com/handbook/business-ops/it-ops-team/access-requests/#single-person-access-request). Relevant channels include:
      - [ ] Email list (Google Groups)
        - engineering@gitlab.com
        - supportteam@gitlab.com
      - Google Drive shared drives (auto access via Google Groups)
        - Support
        - Engineering
      - [ ] 1password Support Vault
      - [ ] In ZenDesk as Staff in Support Group
      - [ ] Slack
        - Join `#support-team-chat`, `#support_gitlab-com`, `#support_self-managed`, `#support-managers`
        - Added to `@support-selfmanaged` group
        - Added to `@support-team-amer`, `@support-team-emea` or `@support-team-apac` group
      - [ ] GitLab groups and aliases:
        - [ ] _Manager:_ add team member to `gitlab-com/support/dotcom` as `Maintainer`
        - [ ] _Manager:_ add team member to `gitlab-com/support` as `Maintainer`
        - [ ] _Manager:_ add team member to regional group `gitlab-com/support/amer`, `gitlab-com/support/apac` or `gitlab-com/support/emea` as `Owner`
   1. [ ] Start Stage 1 here, as well as the first steps of Stage 2 and Stage 3.
   1. [ ] Check back daily on what was blocking you on your General Onboarding Checklist
      until that list is completely done, then focus on this one.
1. [ ] Team Calls: To keep up with the changes and happenings within the Support team, we have team calls every week. Every member of the support team is encouraged to join so they can also state their opinions during the call.
   1. [ ] Read up on the [calls the Support Team uses to sync up](https://about.gitlab.com/handbook/support/#weekly-meetings) and make sure you have the ones that pertain to you on your calendar.
   1. [ ] Identify agendas for those meetings, and read through a few past meetings in the document.
   1. [ ] Verify that you have a 1:1 scheduled with your manager and you have access to the agenda for that meeting.
1. [ ] Introduce yourself to your team!
   1. [ ] Write an entry with your name, location, the date you started, a quick blurb about your experience, personal interests and what drew you to GitLab support in the [Support Week in Review doc](https://docs.google.com/document/d/1eyMzbzImSKNFMpmu33C6imvC1iWEWHREJqaD6mkVDNg/edit) before Friday.
      1. [ ] Manager: Make sure to introduce the team member in the [Engineering Week-in-Review document](https://docs.google.com/document/d/1Oglq0-rLbPFRNbqCDfHT0-Y3NkVEiHj6UukfYijHyUs/edit#) and copy the introduction over to [Support Week in Review](https://docs.google.com/document/d/1eyMzbzImSKNFMpmu33C6imvC1iWEWHREJqaD6mkVDNg/edit#heading=h.pykb7ntmpjic)!
      1. [ ] Manager: Talk to the new team member about doing an introduction at the next regional team meeting, and add it to the agenda.
   1. [ ] Now, also format and send the introduction post you just created to the `#support-team-chat` Slack channel. Welcome to [multi-modal communication](https://about.gitlab.com/handbook/communication/#multimodal-communication), key to effective communication at GitLab!
1. [ ] Manager: add team member to the [Support Shared Drive](https://drive.google.com/drive/folders/0AEeARMMpt4eDUk9PVA) in Google Drive with `Contributor` permissions.

### Stage 1: Become familiar with git and GitLab basics

- [ ] **Done with Stage 1**

_Typically started in the first week, completed by the end of the second week_

If you are already comfortable with using Git and GitLab and you are able to
retain a good amount of information by just watching or reading through, that is
okay. But if you see a topic that is completely new to you, stop the video and try
it out for yourself before continuing.

1. [ ] Let your manager know you're ready to be assigned a trainer.
1. [ ] Just quickly check on your Zendesk account to make sure that is ready for you when you need it.
1. [ ] Add a [profile picture](https://support.zendesk.com/hc/en-us/articles/203690996-Updating-your-user-profile-and-password#topic_rgk_2z2_kh)
   to your Zendesk account
1. [ ] Let your manager know if you were not able to create an account in Zendesk for some reason.
1. [ ] Under your profile in Zendesk, it should read `Support Staff` under role. If it reads `Light Agent`, inform your manager.
1. [ ] Get familiar with our [support workflows](https://about.gitlab.com/handbook/support/workflows/#support_workflows)
1. [ ] [Request access to Chatops](https://docs.gitlab.com/ee/development/chatops_on_gitlabcom.html#chatops-on-gitlabcom)
2. [ ] Manager: Add team member to the Support Team Drive.

Go over these topics in [GitLab University](https://docs.gitlab.com/ee/university/):

1. [ ] Under the topic of [Version Control and Git](https://docs.gitlab.com/ee/university/#1-1-version-control-and-git)
   1. [ ] [About Version Control](https://docs.google.com/presentation/d/16sX7hUrCZyOFbpvnrAFrg6tVO5_yT98IgdAqOmXwBho/edit#slide=id.gd69537a19_0_14)
   1. [ ] [Try Git](https://www.katacoda.com/courses/git)
   1. [ ] Explore [Git internals](https://git-scm.com/book/en/v2/Git-Internals-Plumbing-and-Porcelain)
     and go back to it from time to time to learn more about how Git works
1. [ ] Under the topic of [GitLab Basics](https://docs.gitlab.com/ee/university/#12-gitlab-basics)
   1. [ ] All the [GitLab Basics](http://docs.gitlab.com/ee/gitlab-basics/README.html)
      that you don't feel comfortable with. If you get stuck, see the linked videos
      under GitLab Basics in GitLab University
   1. [ ] [GitLab Flow](https://www.youtube.com/watch?v=UGotqAUACZA)
   1. [ ] Take a look at how the different GitLab versions [compare](https://about.gitlab.com/features/)
1. [ ] Any of these topics that you don't feel comfortable with in the [user training](https://gitlab.com/gitlab-org/gitlab-ee/tree/master/doc/university/training/topics)
   we use for our customers: `env_setup.md`, `feature_branching.md`, `stash.md`, `git_log.md`.
   For the rest of the topics in `user training`, just do a quick read over the file names
   so you start remembering where to find them.
1. [ ] Get familiar with the services GitLab offers
   1. [ ] The differences between [CE and EE (Core, Starter, Premium, Ultimate)](https://about.gitlab.com/pricing/#self-managed)
1. [ ] Get familiar with the [different teams in-charge of every stage in the DevOps cycle](https://about.gitlab.com/handbook/product/categories/#devops-stages) and what they are responsible for. This will assist you in adding the right labels when creating issues and escalating in the right Slack channel.

### Stage 2. Installation and Administration basics.

- [ ] **Done with Stage 2**

_Typically started in the first week, completed by the end of the third week_

**Goals**

- Get your development machine set up.
- Familiarize yourself with the codebase.
- Be prepared to reproduce issues that our users encounter.
- Be comfortable with the different installation options of GitLab and configure LDAP.
- Have an installation available for reproducing customer bug reports.

1. [ ] Check back on your Zendesk account to see if you are an `Support Staff` yet.
1. [ ] Manager: Add team member to the `GitLab Support` Google Calendar.
1. [ ] Add team member to the `GitLab Support` Google Cloud Project with `Support Permissions`.

**Set up your development machine**

1. [ ] Install the [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit)

**Installations**

You will keep one installation continually updated on Digital Ocean (managed through terraform),
just like many of our clients, but you need to choose where you would like to test
other installations. _(TODO: We need to list some benefits of each choice here.)_

1. [ ] Manager: Add new team member to the [dev-resources](https://gitlab.com/gitlab-com/dev-resources) project.
1. [ ] Use terraform to create a new test instance.
    1. [ ] Clone the dev-resources project.
    1. [ ] Read up on [how to create a new instance](https://gitlab.com/gitlab-com/dev-resources/blob/master/dev-resources/README.md).
    1. [ ] Create a new file and name it `<your-full-name>.tf` in a new branch. You
       can copy another team member's file and modify it or you can create your own
       from scratch. If you have any issues or questions ask in the #support channel.
    1. [ ] After you've created it, open up a merge request and if the tests pass then merge it.
1. [ ] Set up your [test environments](https://about.gitlab.com/handbook/support/workflows/support-engineering/general/test_env.html).
1. [ ] Choose between Local VM's or Digital Ocean for your preferred test environment,
   and note it in a comment below.
1. [ ] Perform each of the following [Installation Methods](https://about.gitlab.com/installation/)
   on the test environment you chose above:
   1. [ ] Install via [Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab/)
   1. [ ] Populate with some test data: User account, Project, Issue
   1. [ ] Backup using our [Backup rake task](http://docs.gitlab.com/ee/raketasks/backup_restore.html#create-a-backup-of-the-gitlab-system)
   1. [ ] Install via [Docker](https://docs.gitlab.com/ee/install/docker.html)
   1. [ ] Restore a backup to your Docker VM using our [Restore rake task](http://docs.gitlab.com/ee/raketasks/backup_restore.html#restore-a-previously-created-backup)
   1. [ ] You can check this box but this one never stops as long as you are a Support
      Engineer for GitLab: Keep this installation up-to-date as patch and version
      releases become available, just like our customers would.
1. [ ] Ask as many questions as you can think of in the `#support` chat channel

### Stage 3. Customer Interaction through Zendesk

- [ ] **Done with Stage 3**

_Typically started in the first week, and completed by the end of the fourth week_

**Goals**

- Have a good understanding of ticket flow in Zendesk and how to interact with our various channels.
- See some common issues that our customers face and how to resolve them.

**Initial Zendesk training**

Zendesk is our Support Center and our main communication line with our customers.
We communicate with customers through several other channels too, see the support
handbook for the full list. Set aside around 40 minutes for completion of this section.

1. [ ] Complete Zendesk Agent training
   1. [ ] Navigate to [Zendesk university](http://training.zendesk.com/checkout/3djt0nv8i5y5r) and register.
      You'll receive an email with information on accessing the Zendesk course
   1. [ ] Complete the **"Zendesk Suite Overview: Support"** course (approx. 10 min)
1. [ ] Review additional Zendesk resources
   1. [ ] [UI Overview](https://support.zendesk.com/hc/en-us/articles/203661806-Introduction-to-the-Zendesk-agent-interface)
   1. [ ] [Updating Tickets](https://support.zendesk.com/hc/en-us/articles/212530318-Updating-and-solving-tickets)
   1. [ ] [Working w/ Tickets](https://support.zendesk.com/hc/en-us/articles/203690856-Working-with-tickets)
      *(Read [Avoiding Agent Collisions](https://support.zendesk.com/hc/en-us/articles/203690856-Working-with-tickets#topic_ryy_42g_vt) carefully).*

**Learn about the Support process**

1. [ ] Perform 15 one-hour pairing sessions with Support Engineers. Contact each in
   Slack to find a time that works, then create a Calendar event, inviting that person.
   When it's time for the pairing session, create a new [Support Pairing project Issue](https://gitlab.com/gitlab-com/support/support-pairing)
   and use the **Ticket Pairing** template for the call.
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
1. [ ] Dive into our Zendesk support process by reading how to [handle tickets](https://about.gitlab.com/handbook/support/onboarding/#handling-tickets).
1. [ ] Learn about our [Support Modes of Work](https://about.gitlab.com/handbook/support/workflows/support-modes-of-work.html#first-response-time-hawk).
   - Here you will learn about our four rotating roles so that people know what to do and have variation in their work.
1. [ ] Learn about the [hot queue](https://about.gitlab.com/handbook/support/workflows/working-with-tickets.html#hot-queue-how-gitlab-manages-ticket-assignments).
1. [ ] Learn about the [Zendesk SLA settings and Breach Alerts](https://about.gitlab.com/handbook/support/workflows/working-with-tickets.html#zendesk-sla-settings-and-breach-alerts).
1. [ ] Start getting real world experience by handling real tickets, all the while gaining further experience with GitLab.
   1. [ ] First, learn about our [Support Channels](https://about.gitlab.com/handbook/support/channels).
   1. [ ] Proceed on to [Regular email support tickets](https://about.gitlab.com/handbook/support/channels/#regular-zendesk-tickets).
      - Here you will find tickets from our GitLab EE Customers and GitLab CE Users
      - Tickets here are extremely varied and often very technical
      - You should be prepared for these tickets, given the knowledge gained from the previous steps of your training
1. [ ] Check out your colleagues' responses.
   1. [ ] Hop on to the #zd-self-managed-feed channel in Slack and see the tickets as they
      come in and are updated.
   1. [ ] Read through about 20 old tickets that your colleagues have worked on and their responses.

**Learn about the Escalation process for Issues**

Some tickets need specific knowledge or a deep understanding of a particular component
and will need to be escalated to a Senior Support Engineer or a Developer.

1. [ ] Read about [creating issues](https://about.gitlab.com/handbook/support/onboarding/#create-issues)
   and [issue prioritization](https://about.gitlab.com/handbook/support/workflows/shared/zendesk/setting_ticket_priority.html).
1. [ ] Take a look at the [GitLab.com Team page](https://about.gitlab.com/team/)
   to find the resident experts in their fields.

**Learn about raising issues and fielding feature proposals**

1. [ ] Understand what's in the pipeline at GitLab as well as proposed features: [Direction Page](https://about.gitlab.com/direction/).
1. [ ] Practice searching issues and filtering using [labels](https://gitlab.com/groups/gitlab-org/-/labels)
   to find existing feature proposals and bugs.
1. [ ] If raising a new issue always provide a relevant label and a link to the relevant ticket in Zendesk.
1. [ ] Add [customer labels](https://about.gitlab.com/handbook/support/workflows/issue_escalations.html#adding-labels)
   for those issues relevant to our subscribers.
1. [ ] Take a look at the [existing issue templates](https://gitlab.com/gitlab-org/gitlab/tree/master/.gitlab/issue_templates)
   to see what is expected (look at the comments in the markup for details.)
1. [ ] Raise issues for bugs in a manner that would make the issue easily reproducible.
   A Developer or a contributor may work on your issue.
1. [ ] Schedule a 45 minute call with your trainer where you share your screen
   with them while you answer tickets on Zendesk, and they give you feedback and answer
   your questions. The goal of this call is for your trainer to pass on tactical
   knowledge about the ticket handling process. Repeat this step a few times if it would be beneficial to you.

#### Congratulations. You now have your Zendesk Wings!

From now on you can spend most of your work time answering tickets in Zendesk. Try
to set aside 2 hours per day to make it through **Stage 4-6** of this list. Never
hesitate to ask as many questions as you can think of in the `#support` chat channel.

### Stage 4. GitLab.com Admin & Production Architecture
- [ ] **Done with Stage 4**

_WIP: Add tasks/resources to learn about the setup of GitLab.com, including Shared Runners_

1. [ ] WIP: Review materials relevant to GitLab.com Architecture
   1. [ ] https://about.gitlab.com/handbook/engineering/infrastructure/production-architecture/
   1. [ ] https://www.youtube.com/watch?v=uCU8jdYzpac

#### Monitoring
1. [ ] get familiar with the dashboards in the [private monitoring infrastructure](https://dashboards.gitlab.net)
1. [ ] get familiar with the dashboards in the [public monitoring infrastructure](http://dashboards.gitlab.com)
1. [ ] get familiar with Prometheus [application metrics](https://prometheus-app.gprd.gitlab.net/graph) and [infrastructure metrics](https://prometheus.gprd.gitlab.net/graph), investigate how to [query](https://prometheus.io/docs/querying/basics/) to get information out of these endpoints.
1. [ ] get familiar with `targets` and `alerts` within prometheus.
   - [application targets](https://prometheus-app.gprd.gitlab.net/targets), [infrastructure targets](https://prometheus.gprd.gitlab.net/targets)
   - [application alerts](https://prometheus-app.gprd.gitlab.net/alerts), [infrastructure alerts](https://prometheus.gprd.gitlab.net/alerts)
1. [ ] get familiar with [prometheus alert manager](https://alerts.gprd.gitlab.net), look for the documentation of this in the [runbooks](https://gitlab.com/gitlab-com/runbooks).

#### Reporting
1. [ ] Examine the types of issues typical in the [Production Issue Tracker](https://gitlab.com/gitlab-com/gl-infra/production/issues)
1. [ ] Take a look at the different issue templates in the [Production Issue Tracker](https://gitlab.com/gitlab-com/gl-infra/production/issues)
1. [ ] File your first issue in the [Production Issue Tracker](https://gitlab.com/gitlab-com/gl-infra/production/issues)

#### Production Runbooks
1. clone and get familiar with the [runbooks](https://gitlab.com/gitlab-com/runbooks)

#### Context & Comfort with GitLab's code base:
1. [ ] review issues labeled as `outage` in the infrastructure issue tracker.
1. [ ] read about the [application architecture](https://docs.gitlab.com/ce/development/architecture.html)

#### Bastion setup for SSH:
1. [ ] Set up and register your SSH key with GitLab.com
1. [ ] N: follow the steps in [GPRD bastion hosts](https://gitlab.com/gitlab-com/runbooks/blob/master/howto/gprd-bastions.md)

1. [ ] Manager: fill out an access request form for:
   1. [ ] GitLab.com admin access
   1. [ ] GitLab.com production console access
   1. [ ] GitLab.com database console access

1. [ ] After receiving access, try ssh into a host and make sure it works.


### Stage 5. Gathering Diagnostics

- [ ] **Done with Stage 5**

_Typically done around the third week._

**Goal** Understand the gathering of diagnostics for GitLab instances

1. [ ] Learn about the GitLab checks that are available
    1. [ ] [Environment Information and maintenance checks](https://docs.gitlab.com/ee/administration/raketasks/maintenance.html)
    1. [ ] [GitLab check](https://docs.gitlab.com/ee/administration/raketasks/check.html)
    1. [ ] Omnibus commands
        1. [ ] [Status](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#get-service-status)
        1. [ ] [Starting and stopping services](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#starting-and-stopping)
        1. [ ] [Starting a rails console](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#starting-a-rails-console-session)
1. [ ] Learn about using Sentry to track errors:
    1. [ ] [Sentry, GitLab & You](https://docs.google.com/presentation/d/1j1J4NhGQEYBY8la6lCK-N-bw749TbcTSFTD-ANHiels/edit#slide=id.p)
    1. [ ] [Searching Sentry](https://about.gitlab.com/handbook/support/workflows/500_errors.html#searching-sentry)
1. [ ] Learn about using Kibana to track errors:
    1. [ ] [Kibana Training](https://about.gitlab.com/handbook/support/workflows/500_errors.html#searching-sentry)
    1. [ ] [Searching Kibana](https://about.gitlab.com/handbook/support/workflows/500_errors.html#searching-kibana)



### Stage 6. CI/CD
- [ ] Set up [GitLab CI](https://docs.gitlab.com/ee/ci/quick_start/README.html)

_WIP: Add tasks/resources to learn additional CI/CD_

### Stage 7. On Call Rotations to build .com Expertise

- [ ] **Done with Stage 7**

**Goal** Gain experience in real world .com outages by participating in on-call rotations.

1. [ ] Participate in 5 Shadow Rotations with the on-call SRE
   use the [GitLab.com Support Engineer - On-call Shadowing template](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates/GitLab.com%20Support%20Engineer%20-%20On-call%20Shadowing.md)
     1. [ ] call with ___
     1. [ ] call with ___
     1. [ ] call with ___
     1. [ ] call with ___
     1. [ ] call with ___

### Stage 8. Make the path better for those who come after.
You probably noticed that there was something broken or non-ideal in the onboarding process.
You might have brought it up to your manager.
They likely told you to look at Stage 8 of your onboarding issue.

1. [ ] Make an improvement to the Support Engineer Bootcamp issue.

### Stage 9. Optional Advanced GitLab Topics

Discuss with your training manager if you should stop here and close your issue
or continue. Also, discuss which of the advanced topics should be followed. Do
not do all of them as they might not be relevant to what customers need
right now and can be a significant time investment.

These are some of GitLab's more advanced features. You can make use of
GitLab.com to understand the features from an end-user perspective and then use
your own instance to understand setup and configuration of the feature from an
Administrative perspective

- [ ] Get to know the [GitLab API](https://docs.gitlab.com/ee/api/README.html), its capabilities and shortcomings
- [ ] Create your first [GitLab Page](https://docs.gitlab.com/ee/pages/administration.html)
- [ ] Replicate GitLab.com's setup on a separate instance.
- [ ] Install GitLab from [Source](https://docs.gitlab.com/ee/install/installation.html).
      Installation from source is not common but will give you a greater understanding
      of the components that we employ and how everything fits together.

/label ~bootcamp

/epic &16
