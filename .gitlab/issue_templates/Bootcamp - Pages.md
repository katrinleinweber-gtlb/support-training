**Title:** _"Gitlab Pages Boot Camp - **your-name**"_

### Overview

**Goal**: Learn how to publish sites using GitLab Pages.

*Length* (numbered stages): 2-3 hours (Stage 1) + 1-2 hours (other Stages) = 3-5 hours

**Objectives**: At the end of this bootcamp, you should be able to:

For Self-Hosted Support Engineers only:
  * Configure GitLab Pages in an Omnibus install using wildcard domains.
  * Optional: Configure Pages using a custom domain.

For both Self-Hosted and GitLab.com Services Support Agents:
  * Understand how to trigger Pages to build.
  * Create a project and yml file
  * Have Pages build to a base username or group Pages link.

### Stage 1 (Self Hosted only): Configure GitLab Pages

- [ ] **Done with Stage 1**

From the [GitLab Pages administration Docs Page](https://docs.gitlab.com/ee/administration/pages/index.html):

1. [ ] Read the [Overview](https://docs.gitlab.com/ee/administration/pages/index.html#overview)
   and [Prerequisites](https://docs.gitlab.com/ee/administration/pages/index.html#prerequisites).
1. [ ] Configure GitLab Pages using [Wildcard domains](https://docs.gitlab.com/ee/administration/pages/index.html#configuration).
1. [ ] Verify that it works and add a screenshot in the comments.
1. [ ] Optional: Configure Page [using a custom domain](https://docs.gitlab.com/ee/administration/pages/index.html#advanced-configuration).
1. [ ] Optional: Verify that it works and add a screenshot in the comments.
1. [ ] Optional: Review the other sections of the [GitLab Pages administration Docs Page](https://docs.gitlab.com/ee/administration/pages/index.html)
   not covered above.

_Note: Update the tasks if you decide to do the TLS options._

**WIP: Improvement Required**: Tips on testing this without purchasing a domain.

### Stage 2: See How GitLab Pages Works

- [ ] **Done with Stage 2**

Either in your own instance or on GitLab.com:

1. [ ] Read over the [Getting Started](https://docs.gitlab.com/ee/user/project/pages/) section on the User Documentation.
1. [ ] [Fork an example project](https://gitlab.com/pages) and follow the other instructions to make it work.
1. [ ] Add a link to your working example project here (or add a screenshot in the comments):

Congratulations! You've published your first set of Pages!

### Stage 3: Setup Your Own GitLab Pages

- [ ] **Done with Stage 3**

On GitLab.com (website or command line):

1. [ ] Create a [new project](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_two.html#create-a-project-from-scratch).
1. [ ] Make sure to (re)name it correctly so that it will become your [user site](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_two.html#fork-a-project-to-get-started-from).
   Alternatively, do it for a group.
1. [ ] Create the yml file using the appropriate template. If you're not using a SSG,
   then choose the `HTML` version for hand-written HTML pages.
1. [ ] Once your first pipeline is complete, your site should be accessible. Add the
   link here (or add a screenshot in the comments):

### Stage 4: Tickets

- [ ] **Done with Stage 4**

1. [ ] Answer 5 API tickets and paste the links here. Do this even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers.

   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

### Stage 5: Pair on Customer Calls

- [ ] **Done with Stage 5**

1. [ ] Pair on two diagnostic calls, where a customer is having trouble with GitLab Pages.
   1. [ ] call with ___
   1. [ ] call with ___

### Penultimate Stage: Review

- [ ] **Done with Penultimate Stage**

You feel that you can now do all of the following objectives:
1. [ ] <insert a copy of objectives>

Any updates or improvements needed? If there are any dead links, out of date or inaccurate
content, missing content whether in this bootcamp or in other documentation, list
it below as tasks for yourself!
* [ ] Update ...

### Final Stage: Completion

1. [ ] Have your trainer and manager review this issue.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the bootcamp went once you have reviewed this issue.
1. [ ] Once complete, add this bootcamp to the list of training you have completed.
1. [ ] If you complete all stages of the bootcamp, add "Pages Expert" to your list of expertise on the team page!

/label ~bootcamp

/epic &16
