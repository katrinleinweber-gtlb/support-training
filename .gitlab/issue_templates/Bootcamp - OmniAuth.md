**Title:** _"OmniAuth Boot Camp - **your-name**"_

**Goal of this checklist:** Set a clear path for OmniAuth Expert training

**Objectives**: At the end of this bootcamp, you should be able to:
* Understand how GitLab leverages the OmniAuth gem for user authentication
* Troubleshoot customer's issues with OmniAuth

Remember to contribute to any documentation that needs updating

### Stage 1: Commit and Become familiar with what OmniAuth is

- [ ] **Done with Stage 1**

1. [ ] Ping your manager on the issue to notify them you have started
1. [ ] Commit to this by notifying the current experts that they can start
routing less-technical OmniAuth questions to you
1. [ ] Read through the [GitLab OmniAuth Documentation](https://docs.gitlab.com/ee/integration/omniauth.html)
1. [ ] Read through the [OmniAuth gem documentation](https://github.com/omniauth/omniauth)

### Stage 2: Technical Setup

- [ ] **Done with Stage 2**

1. Implement OmniAuth on a test Omnibus instance

   1. [ ] Choose two OmniAuth Supported Providers from the [list](https://docs.gitlab.com/ee/integration/omniauth.html#supported-providers) and read through their documentation.
   1. [ ] Set up the [initial OmniAuth Configuration](https://docs.gitlab.com/ee/integration/omniauth.html#initial-omniauth-configuration)
   1. [ ] Implement OmniAuth on your test instance using the providers' documentation.

### Stage 3: Tickets

- [ ] **Done with Stage 3**

1. [ ] Go through 10 solved OmniAuth tickets to check the responses and get a sense
of the types of frequently asked questions that come up.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Answer 10 OmniAuth tickets and paste the links here, even if a ticket seems
too advanced for you to answer. Find the answers from an expert and relay them to
the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

### Stage 4: Pair on Customer Calls

- [ ] **Done with Stage 4**

1. [ ] Pair on two Support calls, where a customer has a problem with OmniAuth.
   1. [ ] call with ___
   1. [ ] call with ___

### Penultimate Stage: Review
You feel that you can now do all of the objectives:
1. [ ] Understand how GitLab leverages the OmniAuth gem for user authentication
2. [ ] Troubleshoot customer's issues with OmniAuth

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this bootcamp or in other documentation, list it below as tasks for yourself!
* [ ] Update ...

### Final Stage

- [ ] Have your trainer and manager review this issue.
- [ ] Manager: schedule a call (or integrate into 1:1) to review how the bootcamp went once you have reviewed this issue.
- [ ] Send a MR to declare yourself an **OmniAuth Expert** on the team page
- [ ] Consider the LDAP and/or SAML bootcamps to become a user authentication master

/epic &16
