**Title:** _"Gitlab Kubernetes Boot Camp - **your-name**"_

Tackle stage 1 first and the last stage last, but the others can be completed in
any order you prefer.

**Goal of this checklist:** Set a clear path for Kubernetes Expert training

Remember to contribute to any documentation that needs updating

### Stage 1: Commit and Become familiar with what Kubernetes is

- [ ] **Done with Stage 1**

1. [ ] Ping your manager on the issue to notify him you have started
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical Kubernetes questions to you
1. [ ] Watch [Managing Cloud Native Applications with Kubernetes - End to End](https://www.youtube.com/watch?v=yOg7BxZLwUg&t=64s)
1. [ ] Read [Kubernetes Concepts](https://kubernetes.io/docs/concepts/)
1. [ ] Follow Tutorial: [Kubernetes Basics](https://kubernetes.io/docs/tutorials/kubernetes-basics/)
1. [ ] Take Linux Foundation's [Introduction to Kubernetes course](https://training.linuxfoundation.org/linux-courses/system-administration-training/introduction-to-kubernetes) (Choose the Audit version when asked. Premium is no use to us)
1. [ ] Familiarize yourself with [Kubernetes Operations (kops)](https://github.com/kubernetes/kops)
1. [ ] Familiarize yourself with [Helm](https://docs.helm.sh/)
1. [ ] Google Cloud: Set up a Cluster using [Google Cloud Console & Cloud SDK](https://cloud.google.com/kubernetes-engine/docs/how-to/creating-a-container-cluster);
   you have access to a Google Cloud project called `gitlab-demo` with your `@gitlab.com`
   Google Apps account.

### Stage 2: GitLab & Kubernetes

- [ ] **Done with Stage 2**

1. Read or watch the following:
   1. [ ] [Installing GitLab on a Kubernetes Cluster](https://docs.gitlab.com/ee/install/kubernetes/)
   1. [ ] [Running GitLab Runner on a Kubernetes Cluster](https://docs.gitlab.com/ee/install/kubernetes/gitlab_runner_chart.html)
   1. [ ] [Demo - CI/CD with GitLab in action](https://about.gitlab.com/2017/03/13/ci-cd-demo/)
   1. [ ] [Idea to Production - with GitLab and Kubernetes](https://www.itnotes.de/gitlab/kubernetes/k8s/gke/gcloud/2017/03/05/idea-to-production-with-gitlab-and-kubernetes/)
   1. [ ] [GitLab Helm Charts](https://gitlab.com/charts/charts.gitlab.io)
   1. [ ] [GitLab Cloud Native Charts](https://gitlab.com/charts/gitlab)

### Stage 3: Practical

- [ ] **Done with Stage 3**

1. [ ] Deploy `gitlab-omnibus` on a Kubernetes Cluster
1. [ ] Deploy a GitLab Runner on a Cluster


### Stage 4: Tickets

- [ ] **Done with Stage 4**

1. [ ] Contribute valuable responses on at least 20 Kubernetes tickets, 10 of which should contain reproducible issues. Even if a ticket seems
too advanced for you to answer. Find the answers from an expert and relay it to
the customer.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

1. [ ] Answer 10 Kubernetes tickets and paste the links here, even if a ticket seems too advanced
for you to answer. Find the answers from an expert and relay them to the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

### Stage 5 (WIP): Quiz?

- [ ] **Done with Stage 5**

_Need link to Quiz here_

1. [ ] Quiz answers were checked by a Kubernetes expect (_insert name here_), who said you passed.

### Stage 6: (Optional)

- [ ] **Done with Stage 6**

1. [ ] Take Linux Foundation's [Kubernetes Fundamentals course + certification bundle](https://training.linuxfoundation.org/linux-courses/system-administration-training/kubernetes-fundamentals).
   Check with your training manager to see if this optional (paid) course is worth
   the significant time investment it requires. _Note: Paid courses can be expensed to Gitlab._

### Final Stage

- [ ] Your Manager needs to check this box to acknowledge that you finished
- [ ] Send a MR to declare yourself a **Kubernetes Expert** on the team page

/label ~bootcamp

/epic &16
